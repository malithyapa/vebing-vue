/*
 * Import momentjs for time value manipulation 
 */
var moment = require('moment');

export default {
    namespaced: true,
    state: {
        eventElmnts: [],
        loopbackElmnts:[],
        staticElmnts:[],
        weekDays: [
            {
                "name": "",
                "value": "0",
                "day": "",
                "date": null,
                "today": false,
            },
            {
                "name": "mon",
                "value": "1",
                "day": "Monday",
                "date": null,
                "today": false,
            },
            {
                "name": "tue",
                "value": "2",
                "day": "Tuesday",
                "date": null,
                "today": false,
            },
            {
                "name": "wed",
                "value": "3",
                "day": "Wednesday",
                "date": null,
                "today": false,
            },
            {
                "name": "thu",
                "value": "4",
                "day": "Thursday",
                "date": null,
                "today": false,
            },
            {
                "name": "fri",
                "value": "5",
                "day": "Friday",
                "date": null,
                "today": false,
            },
            {
                "name": "sat",
                "value": "6",
                "day": "Saturday",
                "date": null,
                "today": false,
            },
            {
                "name": "sun",
                "value": "7",
                "day": "Sunday",
                "date": null,
                "today": false,
            },
        ],
        headerRowHeight: 60,
        headerRowHeightMonth: 30,
        weekMultiDayEventRow: {
            height: 0,
        },
        viewSelected: null,
        startOfWeek: null,
        timeZone: null,
        displayYearSpecial: null,
        displayMonthSpecial: null,
        displayYear: null,
        displayMonth: null,
        displayWeek: null,
        displayDay: null,
    },
    mutations: {
        resetState(state, itemData) {
            //console.log("mutation clearEventElmnts: ");
            state.eventElmnts = [];
            state.staticElmnts = [];
            state.loopbackElmnts = [];
            state.weekMultiDayEventRow.height = 0;
        },
        updateWeekMultiDayEventRow(state, itemData) {
            //console.log("mutation updateWeekMultiDayEventRow: ", itemData)
            state.weekMultiDayEventRow.height = itemData;
        },
        updateCalendarState(state, itemData) {
            //console.log("mutation updateCalendarState: ");
            state.viewSelected = itemData.viewSelected;
            state.startOfWeek = itemData.startOfWeek;
            state.displayDay = itemData.displayDay;
            state.displayWeek =  itemData.displayWeek;
            state.displayMonth = itemData.displayMonth;
            state.displayYear = itemData.displayYear;
            state.displayMonthSpecial = itemData.displayMonthSpecial;
            state.displayYearSpecial = itemData.displayYearSpecial;
            state.timeZone = itemData.timeZone;

            // update date of weekDay elements using currentDate
            var currentDate = moment(itemData.currentTime).format('YYYYMMDD');
            var firstDayOfWeek = moment().day(state.startOfWeek).isoWeek(state.displayWeek);
            for (var i = 1; i < state.weekDays.length; i++) {
                state.weekDays[i].date = moment(firstDayOfWeek).add((i-1),'days').format("YYYYMMDD");
                state.weekDays[i].name = moment(firstDayOfWeek).add((i-1),'days').format("ddd");
                state.weekDays[i].day = moment(firstDayOfWeek).add((i-1),'days').format("dddd");
                if (state.weekDays[i].date === currentDate) {
                    state.weekDays[i].today = true;
                }  else {
                    state.weekDays[i].today = false;
                }
            }
        },
        addMonthElmnts(state, itemData) {
            // console.log("mutation addEventElmnts: ", itemData.startTime);
            state.staticElmnts.push(itemData);
        },
        addEventElmnts(state, itemData) {
            // console.log("mutation addEventElmnts: ",itemData.id);
            if (moment(itemData.endTime).diff(moment(itemData.startTime), 'minutes') > (24*60)){
                // add milti day event element
                state.staticElmnts.push(itemData);
            } else {
                var diffDays = moment(itemData.endTime).format("YYYYDDMM") != moment(itemData.startTime).format("YYYYDDMM");
                var diff = moment(itemData.endTime).diff(moment(itemData.startTime), 'minutes');
                if (diff < (60*24) && diffDays){
                    // add loopbackelement only within the week
                    if (state.startOfWeek == "Monday"){
                        // check for loopback element availability depending on startOfWeeK - Monday
                        if (moment(itemData.endTime).isoWeek() == state.displayWeek){
                            var loopbackEvent = Object.assign({},itemData);
                            loopbackEvent.loopback = true;
                            loopbackEvent.hasChild = false;
                            loopbackEvent.hasParent = true;
                            state.loopbackElmnts.push(loopbackEvent);
                        }
                        // add eventelements only within the week
                        if (moment(itemData.startTime).isoWeek() == state.displayWeek){
                            itemData.loopback = false;
                            itemData.hasChild = true;
                            itemData.hasParent = false;
                            state.eventElmnts.push(itemData);
                        }
                    } else if (state.startOfWeek == "Sunday"){
                        // check for loopback element availability depending on startOfWeeK - Sunday
                        var endTimeAdjusted = moment(itemData.endTime).add(1,'days').format();
                        if (moment(endTimeAdjusted).isoWeek()-1 == state.displayWeek){
                            var loopbackEvent = Object.assign({},itemData);
                            loopbackEvent.loopback = true;
                            loopbackEvent.hasChild = false;
                            loopbackEvent.hasParent = true;
                            state.loopbackElmnts.push(loopbackEvent);
                        }
                        // add eventelements only within the week
                        var startTimeAdjusted = moment(itemData.startTime).add(1,'days').format();
                        if (moment(startTimeAdjusted).isoWeek()-1 == state.displayWeek){
                            itemData.loopback = false;
                            itemData.hasChild = true;
                            itemData.hasParent = false;
                            state.eventElmnts.push(itemData);
                        }
                    }
                } else {
                    // add regular event element
                    itemData.loopback = false;
                    state.eventElmnts.push(itemData);
                }         
            }

            // init overlap array
            for (var i in state.eventElmnts){
                state.eventElmnts[i].overlapArr = state.eventElmnts[i].overlapArr ? state.eventElmnts[i].overlapArr = [] : [];
                state.eventElmnts[i].overlapCount = 0;
                state.eventElmnts[i].overlapIndex = null;
            }
            // init overlap array
            for (var i in state.loopbackElmnts){
                state.loopbackElmnts[i].overlapArr = state.loopbackElmnts[i].overlapArr ? state.loopbackElmnts[i].overlapArr = [] : [];
                state.loopbackElmnts[i].overlapCount = 0;
                state.loopbackElmnts[i].overlapIndex = null;
            }
            
            // find overlapping elements and add them to array
            if(state.eventElmnts.length > 1){
                for (var A in state.eventElmnts){
                    var indexA = parseInt(A);
                    for (var indexB = indexA+1; indexB < state.eventElmnts.length; indexB++){
                        //console.log("compare ",indexA, " with ", indexB);
                        var startA = state.eventElmnts[indexA].startTime;
                        var startB = state.eventElmnts[indexB].startTime;
                        var endA = state.eventElmnts[indexA].endTime;
                        var endB = state.eventElmnts[indexB].endTime;
                        var overlapping = false;
                        // case 1: startA < startB && endA > endB
                        if ((moment(startA).diff(moment(startB), 'minutes') < 0) && (moment(endA).diff(moment(endB), 'minutes') > 0)) {
                            overlapping = true;
                        } else 
                        // case 2: startA < startB && endA < startB
                        if ((moment(startA).diff(moment(startB), 'minutes') < 0) && (moment(endA).diff(moment(startB), 'minutes') > 0)) {
                            overlapping = true;
                        } else 
                        // case 3: startA < endB && endA > endB
                        if ((moment(startA).diff(moment(endB), 'minutes') < 0) && (moment(endA).diff(moment(endB), 'minutes') > 0)) {
                            overlapping = true;
                        } else
                        // case 4: startA > startB && endA < endB
                        if ((moment(startA).diff(moment(startB), 'minutes') > 0) && (moment(endA).diff(moment(endB), 'minutes') < 0)) {
                            overlapping = true;
                        }

                        if (overlapping == true) {
                            state.eventElmnts[indexA].overlap = true;
                            state.eventElmnts[indexB].overlap = true;
                            state.eventElmnts[indexA].overlapArr.push(state.eventElmnts[indexB].id);
                            state.eventElmnts[indexB].overlapArr.push(state.eventElmnts[indexA].id);
                        }
                    }
                }
            }

            // find overlapping elements and add them to array
            if(state.loopbackElmnts.length > 1){
                for (var A in state.loopbackElmnts){
                    var indexA = parseInt(A);
                    for (var indexB = indexA+1; indexB < state.loopbackElmnts.length; indexB++){
                        //console.log("compare ",indexA, " with ", indexB);
                        var startA = state.loopbackElmnts[indexA].startTime;
                        var startB = state.loopbackElmnts[indexB].startTime;
                        var endA = state.loopbackElmnts[indexA].endTime;
                        var endB = state.loopbackElmnts[indexB].endTime;
                        var overlapping = false;
                        // case 1: startA < startB && endA > endB
                        if ((moment(startA).diff(moment(startB), 'minutes') < 0) && (moment(endA).diff(moment(endB), 'minutes') > 0)) {
                            overlapping = true;
                        } else 
                        // case 2: startA < startB && endA < startB
                        if ((moment(startA).diff(moment(startB), 'minutes') < 0) && (moment(endA).diff(moment(startB), 'minutes') > 0)) {
                            overlapping = true;
                        } else 
                        // case 3: startA < endB && endA > endB
                        if ((moment(startA).diff(moment(endB), 'minutes') < 0) && (moment(endA).diff(moment(endB), 'minutes') > 0)) {
                            overlapping = true;
                        } else
                        // case 4: startA > startB && endA < endB
                        if ((moment(startA).diff(moment(startB), 'minutes') > 0) && (moment(endA).diff(moment(endB), 'minutes') < 0)) {
                            overlapping = true;
                        }

                        if (overlapping == true) {
                            state.loopbackElmnts[indexA].overlap = true;
                            state.loopbackElmnts[indexB].overlap = true;
                            state.loopbackElmnts[indexA].overlapArr.push(state.loopbackElmnts[indexB].id);
                            state.loopbackElmnts[indexB].overlapArr.push(state.loopbackElmnts[indexA].id);
                        }
                    }
                }
            }
            
            // find overlapping elements and add them to array
            if(state.eventElmnts.length > 1 && state.loopbackElmnts.length > 1){
                for (var A in state.eventElmnts){
                    var indexA = parseInt(A);
                    for (var indexB in state.loopbackElmnts){
                        var startA = state.eventElmnts[indexA].startTime;
                        var startB = state.loopbackElmnts[indexB].startTime;
                        var endA = state.eventElmnts[indexA].endTime;
                        var endB = state.loopbackElmnts[indexB].endTime;
                        var overlapping = false;
                        // case 1: startA < startB && endA > endB
                        if ((moment(startA).diff(moment(startB), 'minutes') < 0) && (moment(endA).diff(moment(endB), 'minutes') > 0)) {
                            overlapping = true;
                        } else 
                        // case 2: startA < startB && endA < startB
                        if ((moment(startA).diff(moment(startB), 'minutes') < 0) && (moment(endA).diff(moment(startB), 'minutes') > 0)) {
                            overlapping = true;
                        } else 
                        // case 3: startA < endB && endA > endB
                        if ((moment(startA).diff(moment(endB), 'minutes') < 0) && (moment(endA).diff(moment(endB), 'minutes') > 0)) {
                            overlapping = true;
                        } else
                        // case 4: startA > startB && endA < endB
                        if ((moment(startA).diff(moment(startB), 'minutes') > 0) && (moment(endA).diff(moment(endB), 'minutes') < 0)) {
                            overlapping = true;
                        }

                        if (overlapping == true) {
                            state.eventElmnts[indexA].overlap = true;
                            state.loopbackElmnts[indexB].overlap = true;
                            state.loopbackElmnts[indexB].overlapArr.push(state.eventElmnts[indexA].id);
                        }
                    }
                }
            }

            // set overlap count
            for (var i in state.eventElmnts) {
                if (state.eventElmnts[i].overlap){
                    //console.log("overlap true", state.eventElmnts[i]);
                    var overlapCount = state.eventElmnts[i].overlapArr.length + 1;
                    for (var j = 0;  j < state.eventElmnts[i].overlapArr.length ; j++){
                        var index = state.eventElmnts.findIndex(x => x.id == state.eventElmnts[i].overlapArr[j]);
                        //console.log(index);
                        if (index != -1) {
                            if (state.eventElmnts[index].overlapCount < overlapCount) {
                                // child elements
                                state.eventElmnts[index].overlapCount = overlapCount;
                                state.eventElmnts[index].overlapIndex = index;
                                // parent element
                                state.eventElmnts[i].overlapCount = overlapCount;
                                state.eventElmnts[i].overlapIndex = index+1;
                            }
                        } else {
                            index = state.loopbackElmnts.findIndex(x => x.id == state.loopbackElmnts[i].overlapArr[j]);
                            if (state.loopbackElmnts[index].overlapCount < overlapCount) {
                                // child elements
                                state.loopbackElmnts[index].overlapCount = overlapCount;
                                state.loopbackElmnts[index].overlapIndex = index;
                                // parent element
                                state.eventElmnts[i].overlapCount = overlapCount;
                                state.eventElmnts[i].overlapIndex = index+1;
                            }
                        }
                        
                    }
                }
            }
        },
        updateEventElmnt(state, itemData) {
            // console.log("mutation updateEventElmnt: ", itemData.id)
            var index = state.eventElmnts.findIndex(x => x.id == itemData.id);
            if (index != -1) {
                // console.log("mutation updateEventElmnt: ", itemData.startTime)
                state.eventElmnts[index].title = itemData.title;
                state.eventElmnts[index].startTime = itemData.startTime;
                state.eventElmnts[index].endTime = itemData.endTime;
                state.eventElmnts[index].users = itemData.users;
                state.eventElmnts[index].owner = itemData.owner;
            }
            // update loopbackElement if available
            var indexLoopback = state.loopbackElmnts.findIndex(x => x.id == itemData.id);
            if (indexLoopback != -1) {
                // console.log("mutation updateLoopbackElmnt: ", itemData.startTime)
                state.loopbackElmnts[indexLoopback].title = itemData.title;
                state.loopbackElmnts[indexLoopback].startTime = itemData.startTime;
                state.loopbackElmnts[indexLoopback].endTime = itemData.endTime;
                state.loopbackElmnts[indexLoopback].users = itemData.users;
                state.loopbackElmnts[indexLoopback].owner = itemData.owner;
            } 
            // update staticElement if available
            var indexStatic = state.staticElmnts.findIndex(x => x.id == itemData.id);
            if (indexStatic != -1) {
                // console.log("mutation updateStaticElmnt: ", itemData.startTime)
                state.staticElmnts[indexStatic].title = itemData.title;
                state.staticElmnts[indexStatic].startTime = itemData.startTime;
                state.staticElmnts[indexStatic].endTime = itemData.endTime;
                state.staticElmnts[indexStatic].users = itemData.users;
                state.staticElmnts[indexStatic].owner = itemData.owner;
            } 
            // console.log("mutation updateEventElmnt: ", state.loopbackElmnts);
        },
        removeEventElmnt(state, itemData){
            // console.log("mutation removeEventElmnt: ", itemData);
            var index = state.eventElmnts.findIndex(x => x.id == itemData);
            if (index != -1) {
                state.eventElmnts.splice(index,1);
            }
        },
        removeLoopbackElmnt(state, itemData){
            // console.log("mutation removeLoopbackElmnt: ", itemData);
            var index = state.loopbackElmnts.findIndex(x => x.id == itemData);
            if (index != -1) {
                state.loopbackElmnts.splice(index,1);
            }
            var index = state.eventElmnts.findIndex(x => x.id == itemData);
            if (index != -1) {
                state.eventElmnts[index].hasChild = false;
            }
        },
        removeAllElmnt(state, itemData){
            // console.log("mutation removeAllElmnt: ", itemData);
            var index = state.staticElmnts.findIndex(x => x.id == itemData);
            if (index != -1) {
                state.staticElmnts.splice(index,1);
            }
            var index = state.loopbackElmnts.findIndex(x => x.id == itemData);
            if (index != -1) {
                state.loopbackElmnts.splice(index,1);
            }
            var index = state.eventElmnts.findIndex(x => x.id == itemData);
            if (index != -1) {
                state.eventElmnts.splice(index,1);
            }
        },
        addLoopbackElement(state, itemData) {
            // console.log("mutation addLoopbackElement: ", itemData);
            if(itemData.action == "top-up"){
                var startTimeInitial = moment(itemData.event.startTime).subtract(1,'days').format("YYYYMMDD") + "2330";
                var startTimeAdjusted = moment(startTimeInitial,"YYYYMMDDHHmm").format();
                itemData.event.startTime = startTimeAdjusted;
                itemData.event.endTime = moment(startTimeAdjusted).add(itemData.duration,'minutes').format();
                itemData.event.loopback = false;
                itemData.event.hasChild = true;
                itemData.event.hasParent = false;
                var loopbackEvent = Object.assign({},itemData.event);
                loopbackEvent.loopback = true;
                loopbackEvent.hasChild = false;
                loopbackEvent.hasParent = true;
                state.loopbackElmnts.push(loopbackEvent);                                                       
            }
            if(itemData.action == "bottom-down"){
                // console.log("mutation addLoopbackElement: ", itemData); 
                var startTimeInitial = moment(itemData.event.startTime).format("YYYYMMDD") + "2330";
                var startTimeAdjusted = moment(startTimeInitial,"YYYYMMDDHHmm").format();
                var endTimeInitial = moment(itemData.event.startTime).add(1,'days').format("YYYYMMDD") + "0030";
                var endTimeAdjusted = moment(endTimeInitial,"YYYYMMDDHHmm").format();
                itemData.event.loopback = false;
                itemData.event.hasChild = true;
                itemData.event.hasParent = false;
                var loopbackEvent = Object.assign({},itemData.event);
                loopbackEvent.startTime = startTimeAdjusted;
                loopbackEvent.endTime = endTimeAdjusted;
                loopbackEvent.loopback = true;
                loopbackEvent.hasChild = false;
                loopbackEvent.hasParent = true;
                state.loopbackElmnts.push(loopbackEvent); 
            }
        },
    },
    actions: {
        
    },
    getters: {
        eventElmntStartTime: (state) => (itemData) => {
            //console.log("getter eventElmntStartTime ",itemData);
            var index = state.eventElmnts.findIndex(x => x.id == itemData);
            return state.eventElmnts[index].startTime;
        },
        eventElmntEndTime: (state) => (itemData) => {
            //console.log("getter eventElmntEndTime ");
            var index = state.eventElmnts.findIndex(x => x.id == itemData);
            return state.eventElmnts[index].endTime;
        },
        staticElmntStartTime: (state) => (itemData) => {
            //console.log("getter staticElmntStartTime ",itemData);
            var index = state.staticElmnts.findIndex(x => x.id == itemData);
            return state.staticElmnts[index].startTime;
        },
        staticElmntEndTime: (state) => (itemData) => {
            //console.log("getter staticElmntEndTime ");
            var index = state.staticElmnts.findIndex(x => x.id == itemData);
            return state.staticElmnts[index].endTime;
        },
        loopbackElmntStartTime: (state) => (itemData) => {
            //console.log("getter loopbackElmntStartTime ",itemData);
            var index = state.loopbackElmnts.findIndex(x => x.id == itemData);
            return state.loopbackElmnts[index].startTime;
        },
        loopbackElmntEndTime: (state) => (itemData) => {
            //console.log("getter loopbackElmntEndTime ");
            var index = state.loopbackElmnts.findIndex(x => x.id == itemData);
            return state.loopbackElmnts[index].endTime;
        },
    },  
}




