import Vue from 'vue';
import Vuex from 'vuex';
import counter from './modules/counter';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        idToken: null,
        userId: null,
        user: null,
        title: null,
        projectId: null,
        vendorId: null,
        calendarId: null,
        permissions: null,
        appMounted: false,
        elements: [{}],
        components: [[]]
    },
    getters,
    mutations,
    actions,
    modules: {
        counter,
    }
});
