import axios from 'axios'

export default {
    initToken ({commit}) {
        commit('saveToken', localStorage['token'])
    },
    signup ({commit, dispatch}, authData) {
        return axios.post('/register', authData)
            .then(response => {
                // commit('authUser', {
                //     token: response.data.token
                // })
                // localStorage.setItem('token', response.data.token);
                return response;
            })
            .catch(error => {
                throw error.response;
            });
    },
    signin ({commit, dispatch}, authData) {
        return axios.post('/login', authData)
            .then(response => {
                commit('authUser', {
                    token: response.data.token,
                })
                localStorage.setItem('token', response.data.token);
                return response;
            })
            .catch(error => {
                throw error.response;
            });
    },
    facebook ({commit, dispatch}, authData) {
        return axios.post('/facebook', authData)
            .then(response => {
                commit('authUser', {
                    token: response.data.token,
                })
                localStorage.setItem('token', response.data.token);
                return response;
            })
            .catch(error => {
                throw error.response;
            });
    },
    logout ({commit}) {
        commit('clearAuthData')
        localStorage.removeItem('token')
    },
    updateElements ({commit}) {

    },
    removeComponent ({commit} , index) {
        commit('removeComponent', index);
    }
};
