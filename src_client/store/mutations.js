export default {
    authUser(state, userData) {
        state.idToken = userData.token
        state.userId = userData.userId
    },
    storeUser(state, user) {
        state.user = user
    },
    storeUserId(state, userId) {
        state.userId = userId
    },
    storeTitle(state, title) {
        state.title = title
    },
    storePermissions(state, keys) {
        state.permissions = keys
    },
    clearAuthData(state) {
        state.idToken = null
        state.userId = null
    },
    storeProjectId(state, project) {
        state.projectId = project
    },
    storeVendorId(state, vendor) {
        state.vendorId = vendor
    },
    storeCalendarId(state, calendar) {
        state.calendarId = calendar
    },
    saveToken(state, token) {
        state.idToken = token
    },
    saveElements(state, elements) {
        state.elements = elements
    },
    saveCards(state, data) {
        state.components[data.index] = data.cards;
    },
};
