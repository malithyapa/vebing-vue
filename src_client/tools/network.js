/**
 * Module dependencies
 */
import axios from 'axios'

/**
 * Services
 */
import { EventBus } from './event_bus'

/**
 * network service provides an interface that is identical to axios, Network
 * should be able to act as a drop in replacement for axios.
 * In addition to wrapping common functionality of axios, network would do error
 * handling, integrated with the message system to alert the user of possible
 * network issues.
 */
export default {
    get: function(loading, url, data) {

        /**
         * increment loading.index to indicate that an asynchronous operation
         * has begun
         * @see template_card
         */
        loading.index++;

        /**
         * creating a new promise to return, which would resolve when the axios
         * promise resolves.
         * however this promise would not immediately reject in case the axios
         * promise rejects
         * NOT IMPLEMENTED network should attempt to access the server a fixed
         * number of times before rejecting the promise, therefore allowing the
         * underlying component to handle the failure
         */
        var p = new Promise((resolve, reject) => {
            var that = this;
            axios.get(url, data)
                .then((response) => {
                    resolve(response);
                    loading.index--;
                    p.done = true;

                })
                .catch((error) => {
                    /**
                     * alert the user of the network issue
                     */
                    EventBus.$emit('show-message', {
                        title: 'Error',
                        msg: error.response.statusText
                    });
                    // reject(error);
                });
        });

        /**
         * check if the promise resolved after 3 seconds, if not alert the user
         * that the network is slow
         */
        setTimeout(function() {
            if(!p.done) {
                EventBus.$emit('show-message', {
                    title: 'Info',
                    msg: 'Server is taking an unusually long time to respond'
                });
            }
        }, 3000);
        return p;
    },
    post: function(loading, url, data) {

        /**
         * increment loading.index to indicate that an asynchronous operation
         * has begun
         * @see template_card
         */
        loading.index++;

        /**
         * creating a new promise to return, which would resolve when the axios
         * promise resolves.
         * however this promise would not immediately reject in case the axios
         * promise rejects
         * NOT IMPLEMENTED network should attempt to access the server a fixed
         * number of times before rejecting the promise, therefore allowing the
         * underlying component to handle the failure
         */
        var p = new Promise((resolve, reject) => {
            var that = this;

            if(url == '') {
                reject('network: Blank url!');
                return;
            }

            axios.post(url, data)
                .then((response) => {
                    response.errors = response.data.errors;
                    response.listeners = response.data.listeners;
                    response.data = response.data.response;

                    resolve(response);

                    loading.index--;
                    p.done = true;
                })
                .catch((error) => {
                    /**
                     * alert the user of the network issue
                     */
                    EventBus.$emit('show-message', {
                        title: 'Error',
                        msg: error.response.statusText
                    });
                    reject(error);
                });
        });

        /**
         * check if the promise resolved after 3 seconds, if not alert the user
         * that the network is slow
         */
        setTimeout(function() {
            if(!p.done) {
                EventBus.$emit('show-message', {
                    title: 'Info',
                    msg: 'Server is taking an unusually long time to respond'
                });
            }
        }, 3000);
        return p;
    }
}
