/**
 * Services
 */
import network from './network'

const mixin = {
    data() {
        return {
            docs: {}
        }
    },
    mounted() {
        if(this.$options.documents) {
            let documentObject = this.$options.documents.apply(this);
            let dataMap = this.$options.dataMap;
            Object.getOwnPropertyNames(documentObject).forEach((doc) => {
                this.docs[doc] = {
                    name: doc,
                    url:  '/generic/' + documentObject[doc].path.replace('.','/'),
                    findOne: {
                        _id: documentObject[doc].id
                    },
                    get: {},
                    modify: {},
                    replace: {}
                }
            })
            let docs = this.docs;

            Object.getOwnPropertyNames(documentObject).forEach((doc) => {
                // seggregate dataMap to seperate docs
                let docMatch = new RegExp('^' + doc + '(.*)');
                Object.getOwnPropertyNames(dataMap).forEach((prop) => {
                    let mapToPath = dataMap[prop];
                    if(typeof mapToPath == 'object') {
                        mapToPath = dataMap[prop].path;
                    }
                    let matched = docMatch.exec(mapToPath);
                    if (matched) {
                        let path = matched[1];
                        if(path.charAt(0) == '.' && path.charAt(1) != '.') {
                            path = path.substr(1);
                        }
                        // update get object
                        if(matched[1] == '') {
                            docs[doc].get['all'] = prop;
                        } else {
                            docs[doc].get[path] = prop;
                        }
                        // update modify object
                        docs[doc].modify[prop] = path;
                        // update replace object
                        if(dataMap[prop].hasOwnProperty('replace')) {
                            let replace = dataMap[prop].replace.apply(this);
                            Object.keys(replace).forEach((key) => {
                                if(key == '.') {
                                    docs[doc].replace[path + '#'] = replace[key];
                                } else {
                                    docs[doc].replace[path + '#' + key] = replace[key]
                                }
                            })
                        }
                    }
                });
                // fetch initial data from the documents
                network.post({}, docs[doc].url, {
                    findOne: docs[doc].findOne,
                    get: docs[doc].get,
                    replace: docs[doc].replace
                }).then((response) => {
                    const assignVariable = function(ref, path, value) {
                        let that = ref;
                        let paths = path.split('.');
                        paths.forEach((segment, index) => {
                            if(index == (paths.length -1)) {
                                if(ref[segment] == undefined) {
                                    that.$set(ref, segment, value);
                                } else {
                                    ref[segment] = value;
                                }
                            } else {
                                if(ref[segment] == undefined) {
                                    that.$set(ref, segment, {});
                                }
                                ref = ref[segment];
                            }
                        });
                    }
                    if(response.errors.length) {
                        console.log(response.errors);
                    } else {
                        Object.keys(dataMap).forEach((key, dataIndex) => {
                            assignVariable(this, key, response.data[dataIndex]);
                        });
                    }
                    if(response.listeners) {
                        docs[doc].listeners = response.listeners;
                        response.listeners.forEach((listener) => {
                            let listen = listener.split(':', 2);
                            this.sockets.subscribe(listen[0], (data) => {
                                Object.keys(this.docs).forEach((doc) => {
                                    let found = false;
                                    Object.getOwnPropertyNames(this.docs[doc].get).some((prop) => {
                                        let regex1 = new RegExp('^' + this.docs[doc].get[prop] + '(.*)').exec(listen[1]);
                                        if(regex1) {
                                            found = true;
                                            let getPath = prop + regex1[1] + data.extension;
                                            if(getPath.charAt(0) == '.' && getPath.charAt(1) != '.') {
                                                getPath = getPath.substr(1).replace('all.', '');
                                            }
                                            let replace = {};
                                            Object.keys(this.docs[doc].replace).forEach((key) => {
                                                let regex2 = new RegExp('^' + key.split('#')[0]).exec(getPath);
                                                if(regex2) {
                                                    replace[key] = this.docs[doc].replace[key];
                                                }
                                            });
                                            getPath = getPath.replace('all.', '');
                                            network.post({}, this.docs[doc].url, {
                                                findOne: this.docs[doc].findOne,
                                                get: {
                                                    [getPath]: null
                                                },
                                                replace: replace
                                            }).then((response) => {
                                                if(!response.errors.length) {
                                                    let localPath = listen[1] + data.extension;
                                                    assignVariable(this, localPath, response.data[0]);
                                                    if(this.$options.hooks && typeof this.$options.hooks[localPath] == 'function') {
                                                        this.$options.hooks[localPath].apply(this, [data.own, data.diff]);
                                                    }
                                                } else {
                                                    console.log(response.errors);
                                                }
                                            })
                                        }
                                        return found;
                                    });
                                    if(!found) {
                                        reject('Cannot resolve path');
                                    }
                                });
                            })
                        });
                    }
                });
            });
        }
    }
}

export default class stPeterVue {
    constructor(options) {

    }
    install(Vue) {
        // mixin mounted method to add automatic sync
        Vue.mixin(mixin)

        const modify = function(that, path, value, modifier) {
            return new Promise((resolve, reject) => {
                Object.keys(that.docs).forEach((doc) => {
                    let found = false;
                    Object.getOwnPropertyNames(that.docs[doc].modify).some((prop) => {
                        let regex1 = new RegExp('^' + prop + '(.*)').exec(path);
                        if(regex1) {
                            found = true;
                            let modifyPath = that.docs[doc].modify[prop] + regex1[1] + modifier;
                            if(modifyPath.charAt(0) == '.' && modifyPath.charAt(1) != '.') {
                                modifyPath = modifyPath.substr(1);
                            }
                            network.post({}, that.docs[doc].url, {
                                findOne: that.docs[doc].findOne,
                                modify: {
                                    [modifyPath]: value
                                }
                            }).then((response) => {
                                if(!response.errors.length) {
                                    resolve();
                                } else {
                                    reject(response.errors)
                                }
                            })
                        }
                        return found;
                    });
                    if(!found) {
                        reject('Cannot resolve path');
                    }
                });
            });
        }

        // add instance method for modifying data
        Vue.prototype.$sset = function (path, value) {
            return modify(this, path, value, '');
        }

        Vue.prototype.$spush = function(path, value) {
            return modify(this, path, value, '+');
        };

        Vue.prototype.$sremove = function(path, value, mode) {
            let modifier = '*';
            if(mode == 1) {
                modifier = '^'
            }
            if(mode == 2) {
                modifier = '-'
            }
            return modify(this, path, value, modifier);
        };

        Vue.prototype.$smove = function(path, index1, index2) {
            return modify(this, path, [index1, index2], '>')
        };
    }
}
