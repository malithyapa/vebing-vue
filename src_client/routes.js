import Login from './components/login.vue';
import App from './components/app.vue';

export const routes = [
	{
		path: '/login',
		component: Login
	},
	{
		path: '/login/:auth',
		component: Login,
		props: true
	},
	{
		path: '/app',
		component: App
	}
];
