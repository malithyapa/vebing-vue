// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import { routes } from './routes'
import { store } from './store/store'
import axios from 'axios'
import VueSocketIO from 'vue-socket.io'
import stPeterVue from './tools/st_peter_vue'
import { EventBus } from './tools/event_bus'

Vue.use(VueRouter);

const router = new VueRouter({
	routes
});

Vue.config.productionTip = false;

// Vue.use(VueResource);

if(process.env.NODE_ENV == 'production') {
	axios.defaults.baseURL = '/api';
} else {
	axios.defaults.baseURL = 'http://127.0.0.1:3000/api';
}

Vue.use(new stPeterVue({}));

Vue.use(new VueSocketIO({
    debug: false,
    connection: 'http://127.0.0.1:3000',
    options: {
       'query': 'token=' + store.state.idToken
    }
}));

axios.interceptors.request.use((config) => {
	// https://jsperf.com/localstorage2/7
	// refer above test case to justify querying the token from the vuex store
	// instead of querying localStorage everytime
	config.headers['Authorization'] = 'bearer ' + store.state.idToken;
	return config;

}, function (error) {
	return Promise.reject(error);
});

// Add a response interceptor
axios.interceptors.response.use((response) => {
	return response;
},(error) => {
	console.log(error.response);
	if(error.response.status == 401) {
		router.push('/login');
		store.commit('clearAuthData');
	}
	return Promise.reject(error);
});

const sockets = {
    connect() {
        const authenticateSocket = function(that) {
            that.$socket.emit('authenticate', {token: that.$store.state.idToken})
            .on('unauthorized', function(msg) {
                console.log("unauthorized: " + JSON.stringify(msg.data));
                throw new Error(msg.data.type);
            });
        }

        if(!this.$store.state.appMounted) {
            EventBus.$on('app-mounted', () => {
                this.$store.state.appMounted = true;
                authenticateSocket(this);
            });
        } else {
            authenticateSocket(this);
        }
    }
}

new Vue({
	el: '#app',
	router,
	store,
    sockets,
	render: h => h(App)
});
