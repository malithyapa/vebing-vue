/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Referenced data files
 */
var formatElements = require('../data/formatElements.js');

var Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var Guest = new Schema({
	name	: String,
	e1		: [Boolean]
});

var format_data = new Schema({
	value: Number,
	events: [Number],
	status: {type: Number, default: -1},
	by: {type: ObjectId, ref: 'User'},
	modified: Date
});

var dates_data = new Schema({
	value: [Date],
	status: {type: Number, default: -1},
	by: {type: ObjectId, ref: 'User'},
	modified: Date
});

var budget_data = new Schema({
	value: Number,
	status: {type: Number, default: -1},
	planned: {type: Number, default: 0},
	actual: {type: Number, default: 0},
	events: [{
		name: String, 			//Name is for the front-end to display withot querying setup in the budget page
		ratio: Number,
		value: Number,
		breakdown: [{
			name: String,
			value: Number,
			ratio: Number,
			fixed: Boolean
		}]
	}],
	by: {type: ObjectId, ref: 'User'},
	modified: Date
});
// how to store shortlisted + confirmed
var guest_data = new Schema({
	value: [Number],
	status: {type: Number, default: -1},
	by: {type: ObjectId, ref: 'User'},
	modified: Date
});

var Project = new Schema({
	partners	: [{type: ObjectId, ref: 'User'}],
	collab		: [{type: ObjectId, ref: 'User'}],
	setup: {
		format	: {
			data	: {type: format_data, default: format_data},
			history	: [format_data]
		},
		dates	: {																// should be replaced with calendar
			data	: {type: dates_data, default: dates_data},
			history	: [dates_data]
		},
		budget	: {
			data	: {type: budget_data, default: budget_data},
			history	: [budget_data]
		},
		guests	: {
			data	: {type: guest_data, default: guest_data},
			history	: [guest_data]
		}
	},
	currency	: String,
	guests		: [Guest],
	created	: Date,
	elements : [{
		action: String,
		status:		String,
		components: [{name: String, attrib: [String]}]
	}],
	subscribed: [{type: ObjectId}]
}, {
	usePushEach: true
});

mongoose.model('project', Project);
