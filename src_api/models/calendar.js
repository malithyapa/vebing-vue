/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Global symbols
 */
const symbols = require('../tools/globalSymbols');

var Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var eventsSchema = new Schema({
	owner: 			{ type: mongoose.Schema.ObjectId, ref: 'User' },
    users:          [ { type: mongoose.Schema.ObjectId, ref: 'User' } ],
    eventType:      { type: String },
    title:          { type: String, required: true, default: "default"},
    description:    { type: String },
    startTime:      { type: String, required: true, default: "default" },
    endTime:        { type: String, required: true, default: "default" },
    allDay:         { type: Boolean },
    repeat:         { type: String },
    location:       { type: String },
	eventColor:     { type: String },
	project:       	{ type: mongoose.Schema.ObjectId, ref: 'Project' },
});

var calendarSchema = new Schema({
    permissions: {
		type: Boolean,	// mock type to bypass mongoose schema validation
		modify: ['user'],
		add: ['user'],
		remove: ['user'],
		read: ['user']
	},
	user:           { type: mongoose.Schema.ObjectId, ref: 'User' },
    events:         [eventsSchema],
    reminders:      [],
}, {
	usePushEach: true
});

mongoose.model('calendar', calendarSchema);
