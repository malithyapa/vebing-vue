var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var userSchema = new mongoose.Schema({
    permissions: {
		type: Boolean,
		modify: ['any'],
		add: ['any'],
		remove: ['any'],
		read: ['any']

	},
    email: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    title: String,
    gender: Boolean,
    hash: String,
    salt: String,
    auth: String,
    authType: String,
    vendor: {type: mongoose.Schema.ObjectId, ref: 'Vendor'},
    project: {type: mongoose.Schema.ObjectId, ref: 'Project'},                  // should users be allowed multiple projects
    elements: [{
        name: String,
        url: String,
        searchable: Boolean
    }],
    rooms: [{
        reference: {type: mongoose.Schema.ObjectId, ref: 'Room'},
        delieverd: Number,
        seen: Number
    }],
    calendar: {type: mongoose.Schema.ObjectId, ref: 'Calendar'},
    permissionKeys: [Number]
}, {
	usePushEach: true
});

userSchema.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64,'sha512').toString('hex');
};

userSchema.methods.validPassword = function(password) {
    if(!this.salt)
        return false;
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64,'sha512').toString('hex');
    return this.hash === hash;
};

userSchema.methods.generateJwt = function() {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        name: this.name,
        exp: parseInt(expiry.getTime() / 1000),
    }, "MY_SECRET");
};

mongoose.model('user', userSchema);
