var mongoose = require('mongoose');

var Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var Room = new Schema({
  name: String,
  messages: [{by: String, format: Number,text: String, time: Date}]
});

mongoose.model('room',Room);

module.exports.Room = Room;
