/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Referenced data files
 */
var serviceData = require('../data/serviceData');

var Schema = mongoose.Schema,
ObjectId = Schema.ObjectId;

var PackageOption = new Schema ({
	permissions: {
		type: Boolean,
		modify: ['staff', '0'],
		add: ['staff', '0'],
		remove: ['staff', '0'],
		read: ['any']
	},
	optionKey: Number,
	optionType: Number,
	defaultOption: Boolean,
	vendorSelected: Boolean,
	addOptions: Boolean,
	numberProvidedVariable: Boolean,
    selectedEvent: Number,
	eventOptions: [String],
	text1: String,
	text2: String,
	text3: String,
	text4: String,
	timeConstrain: Number,
	selectedTCUnit: Number,
	costPerAdditionalTimeUnit: Number,
	options: [String],
	selectedOption: Number,
	addonCost: Number,
	numberProvided: Number,
	numberProvidedUnit: String,
	costPerAdditionalUnit: Number,
	rightOption: Boolean,
	maxAllowed: Number,
	// below values are not saved in the database but are added to ensure
	// reactivity in the frontend
	timeConstrain_: {
		type: Number,
		default: null
	},
	numberProvided_: {
		type: Number,
		default: null
	}
});

var Addon = {
	optionKey: Number,
	optionType: Number,
	defaultOption: Boolean,
	vendorSelected: Boolean,
	addOptions: Boolean,
	numberProvidedVariable: Boolean,
    selectedEvent: Number,
	eventOptions: [String],
	text1: String,
	text2: String,
	text3: String,
	text4: String,
	timeConstrain: Number,
	selectedTCUnit: Number,
	costPerAdditionalTimeUnit: Number,
	options: [String],
	selectedOption: Number,
	addonCost: Number,
	numberProvided: Number,
	numberProvidedUnit: String,
	costPerAdditionalUnit: Number,
	rightOption: Boolean,
	maxAllowed: Number,
	modifies: String,
	addonMaximum: Number,
	addonMultiplier: Number
}

var PackageAddon = new Schema ({
	permissions: {
		type: Boolean,
		modify: ['staff'],
		add: ['staff'],
		remove: ['staff'],
		read: ['any']
	},
	...Addon
});

var SubscriptionAddon = new Schema ({
	permissions: {
		type: Boolean,
		modify: ['...by'],
		add: ['...by'],
		remove: ['...by'],
		read: ['...by']
	},
	...Addon
});

var Subscription = new Schema ({
	permissions: {
		type: Boolean,
		modify: ['.by'],
		add: ['any'],
		remove: ['.by'],
		read: ['.by']
	},
	by: {type: ObjectId, ref: 'User'},
	project: {type: ObjectId, ref: 'Project'},
	addons: [SubscriptionAddon],
	status: Number
});

var Package = new Schema({
	name: String,
	description: String,
	price: {val: Number, currency: String},
	rating: Number,
	images: [{
		type: ObjectId,
		ref: 'Image',
		permissions: {
			modify: ['staff', '0'],
			add: ['staff'],
			remove: ['staff'],
			read: ['any']
		}
	}],
    format: Number,
	options: [PackageOption],
	addons: [PackageAddon],
	subscribed: [Subscription],
    published: {
        type: Boolean,
        default: false
    },
    info: {
        shortlisted: {
            type: Number,
            default: 0
        },
        subscribed: {
            type: Number,
            default: 0
        },
        messages: [{
            state: Number,
            message: String,
            id: Number
        }]
    },
    settings: {
        advanceDefault: {
            type: Boolean,
            default: true
        },
        advanceRequired: {
            type: Boolean,
            default: false
        },
        advancePerc: {
            type: Number,
            default: 20
        },
        advanceAmount: {
            type: Number,
            default: 0
        },
        advancePerc: {
            type: Number,
            default: 20
        },
        balanceDefault: {
            type: Boolean,
            default: true
        },
        balanceDays: {
            type: Number,
            default: 60
        },
        cancellationDefault: {
            type: Boolean,
            default: true
        },
        refundDeadline: {
            type: Number,
            default: 30
        },
        refundPerc: {
            type: Number,
            default: 50
        }
    }
});


var Album = new Schema({
	name: String,
	images: [{type: mongoose.Schema.ObjectId, ref: 'Image'}],
    likes: {
        type: Number,
        default: 0
    },
	likedBy: [{
		type: mongoose.Schema.ObjectId,
		ref: 'User'
	}],
	bookmarkedBy: [{type: mongoose.Schema.ObjectId, ref: 'User'}],
	comments: [{
		name: String,
		comment: String
	}]
});

var Vendor = new Schema({
	permissions: {
		type: Boolean,	// mock type to bypass mongoose schema validation
		modify: ['staff', '0'],
		add: ['staff'],
		remove: ['staff'],
		read: ['any']
	},
	name: {
		type: String,
		default: 'Your company name'
	},
	staff: [{type: ObjectId, ref: 'User'}],
	dp: {type: ObjectId, ref: 'Image'},
	cover: {type: ObjectId, ref: 'Image'},
	gallery: [{type: ObjectId, ref: 'Image'}],
	about: {
		type: String,
		default: 'Express yourself in 60 characters..'
	},
	address: {
		address: {
			type: String,
			default: 'Your Address'
		},
		city: {
			type: String,
			default: 'City'
		},
		state: {
			type: String,
			default: 'State'
		},
		country: {
			type: String,
			default: 'Country'
		},
		postalCode: {
			type: String,
			default: 'Postal Code'
		},
	},
	phone: {
		phone1: {
			type: String,
			default: '+94 Your number'
		},
		phone2: String
	},
	packages: [Package],
    packageSettings: {
        gatewayPayments: {
            type: Boolean,
            default: false
        },
        paypalPayments: {
            type: Boolean,
            default: false
        },
        bankPayments: {
            type: Boolean,
            default: false
        },
        cashPayments: {
            type: Boolean,
            default: true
        },
        // gateway settings
        enforceSSL: {
            type: Boolean,
            default: false
        },
        liveSecretKey: {
            type: String,
            default: 'KEY'
        },
        livePublishKey: {
            type: String,
            default: 'KEY'
        },
        testSecretKey: {
            type: String,
            default: 'KEY'
        },
        testPublicKey: {
            type: String,
            default: 'KEY'
        },
        // paypal settings
        paypalEmail:  {
            type: String,
            default: 'paypal@email.com'
        },
        paypalStyle: {
            type: String,
            default: 'Default'
        },
        ipnVerify: {
            type: Boolean,
            default: false
        },
        // bank settings
        businessName: {
            type: String,
            default: 'Account name'
        },
        account:  {
            type: String,
            default: 'Account number'
        },
        bankName:  {
            type: String,
            default: 'Bank name'
        },
        bankAddress:  {
            type: String,
            default: 'Bank address'
        },
        details:  {
            type: String,
            default: 'Other bank details'
        },
        // cash settings
        storeAddress:  {
            type: String,
            default: 'Store address'
        },
        storeContact:  {
            type: String,
            default: 'Billing contact number'
        },
        // advance payment
        advanceRequired: {
            type: Boolean,
            default: false
        },
        advancePerc: {
            type: Number,
            default: 20
        },
        balanceDays: {
            type: Number,
            default: 60
        },
        // cancellation
        refundDeadline: {
            type: Number,
            default: 30
        },
        refundPerc: {
            type: Number,
            default: 50
        },
        // general settings
        currency: {
            type: String,
            default: 'USD'
        },
        currencyPosition: {
            type: String,
            default: 'After'
        }
    },
	albums: [Album],
	vendorType: {
		type: Number,
		default: 0,
		permissions: {
			modify: ['staff', '0'],
			add: ['staff'],
			remove: ['staff'],
			read: ['any']
		}
	},
	website: {
		type: String,
		default: 'www.yourwebsite.com'
	},
	ratings: {
		value: Number,
		response: Number
	},
	reviews: [{
		by: String,
		review: String,
		rating: Number
	}],
	profile: [{
		name: String,
		data: Object
	}]
}, {
	usePushEach: true
});

Vendor.virtual('allOptions').get(() => {
	return serviceData.photographyOptions.filter(
        (option) => !option.defaultOption);
});

Vendor.virtual('allAddons').get(() => {
	let addonsArray = [];
	serviceData.photographyOptions.forEach((option) => {
		option.addons.forEach((addon) => {
			let optionClone = JSON.parse(JSON.stringify(option));
			if(addon.optionType != -1) {
				optionClone.optionType = addon.optionType;
			}
			optionClone.numberProvided = 1;
			optionClone.timeConstrain = 1;
			optionClone.addonMaximum = 1;
			optionClone.addonMultiplier = 1;
			optionClone.modifies = addon.modifies;
			addonsArray.push(optionClone);
		});
	});
	return addonsArray;
});


mongoose.model('vendor', Vendor);
