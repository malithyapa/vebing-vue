/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

var Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var Image = new Schema({
	permissions: {
		type: Boolean,	// mock type to bypass mongoose schema validation
		modify: ['any'],
		add: ['any'],
		remove: ['any'],
		read: ['any']
	},
	src: String,
	by:	{type: mongoose.Schema.ObjectId, ref: 'User'},
	vendor: {type: mongoose.Schema.ObjectId, ref: 'Vendor'},
	likes: {
        type: Number,
        default: 0
    },
	likedBy: [{
		type: mongoose.Schema.ObjectId,
		ref: 'User',
		permissions: {
			modify: ['any'],
			add: ['any'],
			remove: ['any'],
			read: ['any']
		}
	}],
	bookmarkedBy: [{type: mongoose.Schema.ObjectId, ref: 'User'}],
	location: String,
	caption: String,
	comments: [{
		name: String,
		comment: String
	}],
	album: {type: mongoose.Schema.ObjectId, ref: 'Album'},
	sample: Boolean
}, {
	usePushEach: true
});

mongoose.model('image', Image);
