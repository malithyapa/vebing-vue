/**
* Module dependencies.
*/
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var user_model = mongoose.model('user');
var passportJWT = require("passport-jwt");
var ExtractJWT = passportJWT.ExtractJwt;
var JWTStrategy   = passportJWT.Strategy;

/**
* Local strategy for passport
* This configuration manages the users that are signed up locally on vebing
*/
passport.use(new LocalStrategy({
        usernameField: 'email'
    },
    function(username, password, done) {
        user_model.findOne({ email: username }, function (err, user) {
            if (err) { return done(err); }
            // Return if user not found in database
            if (!user) {
                return done(null, false, {
                    message: 'User not found'
                });
            }
            // Return if password is wrong
            if (!user.validPassword(password)) {
                return done(null, false, {
                    message: 'Password is wrong'
                });
            }
            // If credentials are correct, return the user object
            return done(null, user);
        });
    }
));

/**
* JWT strategy for passport
* This configuration manages the authentication of web tokens
*/
passport.use(new JWTStrategy({
        // creates a new extractor that looks for the JWT in the authorization header with the scheme 'bearer'
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : "MY_SECRET",
        passReqToCallback: true
    },
    function (req, jwtPayload, done) {
        // verify callback function invoked when authentication is sucessful
         user_model.findById(jwtPayload._id,(err, user) => {
            if(err)
                return done(err, false);
            if(user)
                return done(null, user);
            else
                return done(null, false);
        });
    }
));
