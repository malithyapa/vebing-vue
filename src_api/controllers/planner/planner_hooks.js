/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Extract db models from mongoose
 */
var project_model = mongoose.model('project');

/**
 * This is a sample hook created for testing that hooks are working for the
 * project document.
 * This should trigger everytime a new project is created
 *
 * @param  {String Array} ['created']     watch for changes in 'created' property
 * @param  {function} function      callback to be executed on change detection
 * @return {Promise} Promise        promise returned
 */

project_model.createHook(['created'], function(vendor, matchedRule, indices) {
    return new Promise(function(resolve, reject) {
        resolve();
    });
});
