/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Extract db models from mongoose
 */

var project_model = mongoose.model('project');


// START - UTILITY FUNCTIONS

/**
 * initVendor - this function is invoked by the authentication API to create
 * a new Project object
 * @returns {Object Id} the ID of the created object
 */
module.exports.initProject = function() {

    var project = new project_model();

	project.save(function(err) {
    	if(err) {
            console.log(err);
        }
	});

	return project._id;

};
