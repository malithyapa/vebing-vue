/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Referenced data files
 */
var plannerData = require('../../data/vendorData');

/**
 * Extract db models from mongoose
 */
var user_model = mongoose.model('user');
var vendor_model = mongoose.model('vendor');
var project_model = mongoose.model('project');
var image_model = mongoose.model('image');

/**
 * Register vendor hooks
 */
var plannerHooks = require('./planner_hooks');

// START - BOARD APIs

/**
 * getNi - expressjs middleware function that retrieves the cards to be
 * displayed in a board thats not yet implemented
 * NOT FULLY IMPLEMENTED only returns a dummy set of cards
 * @param {Object} req.user user document from the jwt token
 * @returns {Object} card names and card data for the dashboard
 */
module.exports.getNi = function(req, res) { // NOT IMPLEMENTED
    res.status(200).json([{
        name: 'welcome-planner',
        index: 0,
        show: false,
        height: 0
    },
    {
        name: 'blank-space',
        index: 1,
        show: false,
        height: 0
    },
    {
        name: 'blank-space',
        index: 2,
        show: false,
        height: 0
    }]);
}

/**
 * getPackages - expressjs middleware function that retrieves the cards to be
 * displayed in the packages board
 *
 * NOTE: Currently this board displays all the packages that are registered in the
 * database. Filtering based location/formats/dates/vendor type/etc needs to be
 * implemented in the future
 *
 * @param {Object} req.user user document from the jwt token
 * @returns {Object} card names and card data for the dashboard
 */
module.exports.getPackages = function(req, res) { // NOT IMPLEMENTED
    vendor_model.aggregate([
        {$unwind: {
            path: '$packages',
            includeArrayIndex: 'packageIndex'
        }},
        {$match: {
            'packages.published': true
        }}
    ])
    .exec(function(err, packages) {
        var outputCards = [];

        packages.forEach((package, index) => {
            outputCards.push({
                name: 'package-card',
                index: index,
                show: false,
                height: 0,
                data: {
                    id: package.packages._id
                }
            });
        });

        var outputIndex = (packages.length-1);

        outputCards.push({
            name: 'blank-space',
            index: ++outputIndex,
            show: false,
            height: 0
        });

        res.status(200).json(outputCards);
    });
}

module.exports.findPackages = function(req, res) {

    var aggregate = [];

    aggregate.push({$match: {
        'vendorType': req.body.search.selectedVendor
    }});

    aggregate.push({$unwind: {
        path: '$packages'
    }});

    if(req.body.search.maxPrice != null) {
        aggregate.push({$match: {
            'packages.price.val': {
                '$lte': req.body.search.maxPrice
            }
        }});
    }

    if(req.body.search.selectedFormat != null) {
        aggregate.push({$match: {
            'packages.format': req.body.search.selectedFormat
        }});
    }

    var gte = ['timeConstrain', 'numberProvided'];
    var match = ['text1', 'text2', 'text3', 'selectedOption', 'selectedTCUnit',
        'selectedEvent', 'numberProvidedUnit'];

    var searchable = match.concat(gte);

    if(req.body.search.options != null) {
        req.body.search.options.forEach((option) => {
            let elemMatch = {};
            Object.getOwnPropertyNames(option).forEach((prop) => {
                if(searchable.includes(prop)) {
                    if(gte.includes(prop)) {
                        elemMatch[prop] = {
                            '$gte': option[prop]
                        };
                    } else {
                        elemMatch[prop] = option[prop];
                    }
                }
            });
            aggregate.push({
                '$match': {
                    'packages.options': {
                        '$elemMatch': elemMatch
                    }
                }
            });
        });
    }

    vendor_model.aggregate(aggregate).exec(function(err, packages) {
        var outputCards = [];

        packages.forEach((package, index) => {
            outputCards.push({
                name: 'package-card',
                index: index,
                show: false,
                height: 0,
                data: {
                    id: package.packages._id
                }
            });
        });

        var outputIndex = (packages.length-1);

        if(outputCards.length == 0) {
            outputCards.push({
                name: 'welcome-planner',
                index: ++outputIndex,
                show: false,
                height: 0,
                data: {
                    msg: 'Oops, 404 package not found!'
                }
            });
        }

        outputCards.push({
            name: 'blank-space',
            index: ++outputIndex,
            show: false,
            height: 0
        });

        res.status(200).json(outputCards);
    });

}

/**
 * getPackages - expressjs middleware function that retrieves the cards to be
 * displayed in the packages board
 *
 * NOTE: Currently this board displays all the packages that are registered in the
 * database. Filtering based location/formats/dates/vendor type/etc needs to be
 * implemented in the future
 *
 * @param {Object} req.user user document from the jwt token
 * @returns {Object} card names and card data for the dashboard
 */
module.exports.getShortlisted = function(req, res) { // NOT IMPLEMENTED
    project_model.findById(req.user.project)
    .exec(function(err, project) {
        var outputCards = [];

        project.subscribed.forEach((package, index) => {
            outputCards.push({
                name: 'package-card',
                index: index,
                show: false,
                height: 0,
                data: {
                    id: package
                }
            });
        });

        var outputIndex = (project.subscribed.length-1);

        if(outputCards.length == 0) {
            outputCards.push({
                name: 'welcome-planner',
                index: ++outputIndex,
                show: false,
                height: 0,
                data: {
                    msg: 'You forgot to shortlist!'
                }
            });
        }

        outputCards.push({
            name: 'blank-space',
            index: ++outputIndex,
            show: false,
            height: 0
        });

        res.status(200).json(outputCards);
    });
}

// END - BOARD APIs
