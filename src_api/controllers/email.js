var nodemailer = require('nodemailer');

var server = 'http://vebing.com/';
if (process.env.NODE_ENV === 'dev') {
  server = 'localhost/';
}

var smtpConfig = {
  host: 'smtp.gmail.com',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'webbing.register@gmail.com',
    pass: 'emailwebbing'
  }
};

var transporter = nodemailer.createTransport(smtpConfig);

transporter.verify(function(error, success) {
 if (error) {
    console.log('Couldn\'t connect to smtp server');
 } else {
    console.log('Server is ready to take our messages');
 }
});

module.exports.testEmail = function() {

  var mailOptions = {
    from: '"vebing register 👥" <webbing.register@gmail.com>', // sender address
    to: 'malithyapa@gmail.com', // list of receivers
    subject: 'Hello from vebing', // Subject line
    text: 'This works!', // plaintext body
    html: '<b>This works!!</b>' // html body
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if(error) {
      return console.log(error);
    }
    console.log('Message sent: ' + info.response);
  });

};

module.exports.activationEmail = function(email,uuid) {
  var originalEmail = '';
  if (process.env.NODE_ENV === 'production') { //disable automated email authentication for production build
    originalEmail = email;
    email = 'malithyapa@gmail.com';
  }
  var mailOptions = {
    from: '"Webbing Register 👥" <webbing.register@gmail.com>', // sender address
    to: email, // list of receivers
    subject: 'Hello from vebing!', // Subject line
    text: 'Welcome to vebing' + originalEmail + ', Please follow the link below to activate your account..', // plaintext body
    html: '<b>Welcome to vebing, Please follow the link below to activate your account..</b><br><a href="' + server + 'api/activate?id=' + uuid + '">' + server + 'api/activate?id=' + uuid + '</a>' // html body
  };

  console.log('http://localhost/api/activate?id=' + uuid);

  // transporter.sendMail(mailOptions, function(error, info) {
  //   if(error) {
  //     return console.log(error);
  //   }
  //   console.log('Message sent: ' + info.response);
  // });

};

module.exports.joinEmail = function(email, name) {
  console.log('sending email to ' + email);
  var mailOptions = {
    from: '"vebing Register 👥" <webbing.register@gmail.com>', // sender address
    to: email, // list of receivers
    subject: 'Hello from vebing!', // Subject line
    text: 'Welcome to vebing, Please follow the link below to activate your account..', // plaintext body
    html: '<b>' + name + ' has invited you to start planning your wedding. Go to vebing and create a free account!</b><br><a href="' + server + '"> vebing</a>' // html body
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if(error) {
      return console.log(error);
    }
    console.log('Message sent: ' + info.response);
  });

};
