/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Referenced data files
 */
var serviceData = require('../../data/serviceData');
var vendorData = require('../../data/vendorData');

/**
 * Extract db models from mongoose
 */
var user_model = mongoose.model('user');
var vendor_model = mongoose.model('vendor');
var project_model = mongoose.model('project');
var image_model = mongoose.model('image');

/**
 * Internal tool libraries
 */
var stPeter = require('../../tools/saintPeter');
const symbols = require('../../tools/globalSymbols');

/**
 * addImage - express js middleware function that will add a new image to the
 * specified package
 * image has to be first extracted from the multipart/form processing middleware
 * then resized and saved using sharp middleware before being passed to this
 * function
 * @param {Object} req.user user document from the jwt token
 * @param  {String} res.fields.packageIndex index of the package to which
 * images are to be added
 * @param  {String} res.fields.imageIndex if an imageIndex is specified
 * the image at that index will be replaces
 * @param  {String} res.image._id image id from sharp middleware
 */
module.exports.addImage = function(req,res) {
    vendor_model.findById(req.user.vendor, function(err, vendor) {

        // add original document to itself for arrayDiff calculation
        vendor[symbols.original] = vendor.toObject();
        
        if(req.fields.imageIndex) {
            vendor.packages.some((package, index) => {
                if(package._id == req.fields.packageId) {
                    vendor.packages[index].images
                        .set(req.fields.imageIndex, res.image._id);
                }
            });
        } else {
            vendor.packages.some((package, index) => {
                if(package._id == req.fields.packageId) {
                    vendor.packages[index].images
                        .push(res.image._id);
                }
            });
        }

        vendor.save(function(err) {
            if(err)
            console.log(err);
        });
    });
    res.sendStatus(200);
};
