/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Referenced data files
 */
var serviceData = require('../../data/serviceData');
var vendorData = require('../../data/vendorData');

/**
 * Extract db models from mongoose
 */
var user_model = mongoose.model('user');
var vendor_model = mongoose.model('vendor');
var project_model = mongoose.model('project');
var image_model = mongoose.model('image');

/**
 * Register vendor hooks
 */
var vendorHooks = require('./vendor_hooks');

// START - BOARD APIs

/**
 * getProfile - expressjs middleware function that returns the cards to be
 * loaded for the 'Profile' board
 * @param {Object} req.user user document from the jwt token
 * @returns {Object} cards names and card data from the vendor's profile object
 */
module.exports.getProfile = function(req, res) {
    vendor_model.findById(req.user.vendor, function(err, vendor) {
        var outputCards = [];
        var cardIndex = 0;

        vendor.profile.map((value, index) => {
            let card = {};

            // copy data into the card object
            card.name = value.name;
            card.data = value.data || {};

            // copy profile element id to data for profile element manipulation
            card.data.elementId = value._id;

            // if the data object contains an id property use that as the index
            // else use the card order
            if(value.data && value.data.id) {
                card.index = value.data.id;
            } else {
                card.index  = ++cardIndex;
            }

            // adding properties to ensure reactivity in vue
            card.height = 0;
            card.show = false;

            // make all cards from index 1 movable
            if(index>1) {
                card.movable = true;
            }

            outputCards.push(card);
        });

        outputCards.push({
            name: 'add-profile-item',
            index: ++cardIndex,
            show: false,
            height: 0
        });

        outputCards.push({
            name: 'blank-space',
            index: ++cardIndex,
            show: false,
            height: 0
        });

        res.status(200).json(outputCards);
    });
}

/**
 * setProfile - expressjs middleware function that modifies the data for a
 * particular card in the profile. this is currently used only to modify the
 * color property of the seperator card
 * @param {Object} req.user user document from the jwt token
 * @param {Number} req.body.index index of the card to be modified
 * @param {Object} req.body.data data object to be added to the card
 */
module.exports.setProfile = function(req, res) {
    vendor_model.findById(req.user.vendor, function(err, vendor) {
        vendor.profile[req.body.index].data = req.body.data;
        vendor.markModified('profile');
        vendor.save(function(err) {
            if(err) {
                console.log(err);
            }
            else {
                res.status(200).json({});
            }
        });
    });
}

/**
 * getDashboard - expressjs middleware function that retrieves the cards to be
 * displayed in the dashboard
 * NOT FULLY IMPLEMENTED only returns a dummy set of cards
 * @param {Object} req.user user document from the jwt token
 * @returns {Object} card names and card data for the dashboard
 */
module.exports.getDashboard = function(req, res) { // NOT IMPLEMENTED
    vendor_model.findById(req.user.vendor, function(err, vendor) {
        res.status(200).json([{
            name: 'welcome-vendor',
            index: 0,
            show: false,
            height: 0
        },
        {
            name: 'blank-space',
            index: 1,
            show: false,
            height: 0
        },
        {
            name: 'blank-space',
            index: 2,
            show: false,
            height: 0
        }]);
    });
}

/**
 * getPackages - expressjs middleware function that retrieves the cards to be
 * displayed in the packages
 * NOT FULLY IMPLEMENTED only returns a dummy set of cards
 * @param {Object} req.user user document from the jwt token
 * @returns {Object} card names and card data for the dashboard
 */
module.exports.getPackages = function(req, res) { // NOT IMPLEMENTED
    vendor_model.findById(req.user.vendor, function(err, vendor) {

        var outputCards = [];
        var cardIndex = 0;

        vendor.packages.forEach((package, index) => {
            outputCards.push({
                name: 'package-card-vendor',
                index: package._id,
                show: false,
                height: 0,
                data: {
                    id: package._id
                }
            });

          //  res.status(200).json(outputCards);
            cardIndex = index;
        });

        outputCards.push({
            name: 'add-package',
            index: ++cardIndex,
            show: false,
            height: 0
        });

        outputCards.push({
            name: 'package-settings-card',
            index: ++cardIndex,
            show: false,
            height: 0
        });

        outputCards.push({
            name: 'blank-space',
            index: ++cardIndex,
            show: false,
            height: 0
        });

        res.status(200).json(outputCards);
    });
}

/**
 * getGallery - expressjs middleware function that retrieves the cards to be
 * displayed in the gallery
 * NOT FULLY IMPLEMENTED only returns a dummy set of cards
 * @param {Object} req.user user document from the jwt token
 * @returns {Object} card names and card data for the dashboard
 */
module.exports.getGallery = function(req, res) { // NOT IMPLEMENTED
    //user_model.findById(req.payload._id, function(err, user) {
    vendor_model.findById(user.vendor, function(err, vendor) {

        var outputCards = [];
        var cardIndex = 0;

        /**
         * Send all existing albums to the board
         */
        vendor.albums.forEach((album, index) => {
            outputCards.push({
                name: 'album-card',
                index: album._id,
                show: false,
                height: 0,
                data: {
                    id: album._id,
                }
            });
            cardIndex++;
        });

        outputCards.push({
            name: 'add-album',
            index: ++cardIndex,
            show: false,
            height: 0
        });

        outputCards.push({
            name: 'blank-space',
            index: ++cardIndex,
            show: false,
            height: 0
        });

        res.status(200).json(outputCards);
    });
}

/**
 * getCalendar - expressjs middleware function that retrieves the cards to be
 * displayed in the calendar
 * NOT FULLY IMPLEMENTED only returns a dummy set of cards
 * @param {Object} req.user user document from the jwt token
 * @returns {Object} card names and card data for the dashboard
 */
module.exports.getCalendar = function(req, res) {
    user_model.findById(req.user.id, function(err, user) {
        var outputCards = [];
        var cardIndex = 0;

        outputCards.push({
            name: 'calendar-card',
            index: ++cardIndex,
            show: false,
            height: 0,
            data: {
                id: user.calendar
            }
        });

        outputCards.push({
            name: 'blank-space',
            index: ++cardIndex,
            show: false,
            height: 0
        });

        res.status(200).json(outputCards);
    });
}

// END - BOARD APIs
