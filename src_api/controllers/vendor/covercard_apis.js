/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Other referenced controllers
 */
var vendorUtility = require('./vendor_utility');

/**
 * Referenced data files
 */
var serviceData = require('../../data/serviceData');
var vendorData = require('../../data/vendorData');

/**
 * Extract db models from mongoose
 */
var user_model = mongoose.model('user');
var vendor_model = mongoose.model('vendor');
var project_model = mongoose.model('project');
var image_model = mongoose.model('image');


// START - CARD APIs

/**
 * saveDp - express js middleware function that will save a new image as
 * vendor dp
 * image has to be first extracted from the multipart/form processing middleware
 * then resized and saved using sharp middleware before being passed to this
 * function
 * @todo name of the image to be uploaded can be encoded in the url or sent as
 * data therefore removing the need for the next middleware function
 * @param {Object} req.user user document from the jwt token
 * @param  {String} req.image._id image id from sharp middleware
 */
module.exports.saveDp = function(req, res) {
    vendor_model.findById(req.user.vendor, function(err, vendor) {
        vendor.dp = res.image._id;
        vendor.save(function(err) {
            if(err)
                console.log(err);
        });
    });
    res.sendStatus(200);
};

/**
 * saveCover - express js middleware function that will save a new image as
 * vendor cover picture
 * image has to be first extracted from the multipart/form processing middleware
 * then resized and saved using sharp middleware before being passed to this
 * function
 * @param {Object} req.user user document from the jwt token
 * @param  {String} req.image._id image id from sharp middleware
 */
module.exports.saveCover = function(req,res) {
    vendor_model.findById(req.user.vendor, function(err, vendor) {
        vendor.cover = res.image._id;
        vendor.save(function(err) {
            if(err)
            console.log(err);
        });
    });
    res.sendStatus(200);
};
