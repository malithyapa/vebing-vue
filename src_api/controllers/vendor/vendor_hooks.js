/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Referenced data files
 */
var serviceData = require('../../data/serviceData');

/**
 * Extract db models from mongoose
 */
var user_model = mongoose.model('user');
var project_model = mongoose.model('project');
var image_model = mongoose.model('image');
var vendor_model = mongoose.model('vendor');

/**
 * Reference DB Controllers
 */
var vendorDB = require('../../models/vendor');

/**
 * Global symbols
 */
var symbols = require('../../tools/globalSymbols');
/**
 * External libraries
 */
var _ = require('lodash');

/**
 * Create sample packages on vendor type selection
 */
vendor_model.createHook(['vendorType'], function(vendor, matchedRule, indices) {
    return new Promise(function(resolve, reject) {
        // recover sample images to populate the sample package
        image_model.aggregate([
            {$match: {
                'sample': true
            }}
        ]).exec(function(err, images) {
            serviceData.photographyPackages.forEach((element, index) => {
                vendor.packages.set(index, element);
                for(var i=0;i<5;i++) {
                    var imageIndex = Math.floor(Math.random()*images.length);
                    vendor.packages[index].images.set(i,images[imageIndex]);
                    images.splice(imageIndex,1);
                }
            });
            resolve();
        });
    });
});


/* Album Initilisation hook */
vendor_model.createHook(['albums'], function(vendor, matchedRule, indices, arrayDiff) {
    return new Promise(function(resolve, reject) {
        let promises = [];
        arrayDiff.added.forEach((index) => {
            promises.push(new Promise((resolveP, rejectP) => {
                image_model.aggregate([
                    {$match: {
                        'sample': true
                    }}
                ]).exec(function(err, images) {
                    vendor.albums[index].name = "Album name";
                    for(var i=0;i<5;i++) {
                        var imageIndex = Math.floor(Math.random()*images.length);
                        vendor.albums[index].images.set(i,images[imageIndex]);

                        images.splice(imageIndex,1);
                    }
                    resolveP();
                });
            }));
        })


        Promise.all(promises).then(() => {
            resolve();
        })

    });
});

/**
 * Initialize new packages
 */
vendor_model.createHook(['packages'], function(vendor, matchedRule, indices, arrayDiff) {
    return new Promise(function(resolve, reject) {
        let promises = [];
        arrayDiff.added.forEach((newIndex) => {
            if(vendor.packages[newIndex].options.length == 0) {
                promises.push(new Promise((resolveP, rejectP) => {
                    image_model.aggregate([
                        {$match: {
                            'sample': true
                        }}
                    ]).exec(function(err, images) {
                        vendor.packages.set(newIndex, serviceData.photographyNewPackage);
                        for(var i=0;i<5;i++) {
                            var imageIndex = Math.floor(Math.random()*images.length);
                            vendor.packages[newIndex].images.set(i,images[imageIndex]);
                            images.splice(imageIndex,1);
                        }
                        resolveP();
                    });
                }));
            }
        })

        Promise.all(promises).then(() => {
            resolve();
        })

    });
});

vendor_model.createHook(['packages.*.options', 'packages.*.format'], function(vendor, matchedRule, indices) {
    return new Promise(function(resolve, reject) {
        console.log('options hook running');
        /**
         * This hook ensures the legality of options within a package
         * Example:
         * If an option has the maxAllowed property set to 0 then this hook
         * would replicate that event once for each event
         */
        var eventOptions = [
            ['Ceremony'],
            ['Ceremony', 'Reception'],
            ['Reception']
        ];
        var newOptions = [];
        var ignore = [];
        var added = [];
        vendor.packages[indices[0]].options.forEach((option) => {
            if(!ignore.includes(option.optionKey)) {
                if(option.maxAllowed === 0) {
                    for(var i=0;i<eventOptions[vendor.packages[indices[0]].format].length;i++) {
                        let existing = 0;
                        let exists = vendor.packages[indices[0]].options.some((option2, index) => {
                            existing = index;
                            return ((option2.optionKey == option.optionKey) && (option2.selectedEvent == i))
                        });
                        if(!exists) {
                            var clone = JSON.parse(JSON.stringify(option));
                            clone.selectedEvent = i;
                            newOptions.push(clone);
                        } else {
                            newOptions.push(vendor.packages[indices[0]].options[existing]);
                        }
                    }
                    ignore.push(option.optionKey);
                } else {
                    var found = false;
                    newOptions.forEach((newOption) => {
                        newOptionCopy =JSON.parse(JSON.stringify(newOption));
                        optionCopy =JSON.parse(JSON.stringify(option));
                        delete newOptionCopy['_id'];
                        delete optionCopy['_id'];
                        delete newOptionCopy['numberProvided'];
                        delete optionCopy['numberProvided'];
                        if(_.isEqual(newOptionCopy, optionCopy)) {
                            newOption.numberProvided += option.numberProvided;
                            found = true;
                        }
                    });
                    if(!found) {
                        newOptions.push(option);
                    }
                }
            }
        });
        newOptions.sort((a, b) => (a.optionKey-b.optionKey));
        vendor.packages[indices[0]].options = newOptions;
        resolve();
    });
})


/**
 * Update user project when a new subscription is added
 */
vendor_model.createHook(['packages.*.subscribed'], function(vendor, matchedRule, indices, arrayDiff) {
    return new Promise(function(resolve, reject) {

        if(arrayDiff.added.length>0) {
            let subscription = vendor.packages[indices[0]].subscribed[arrayDiff.added[0]];
            subscription.status = 0;
            user_model.findById(subscription.by, function(err, user) {
                project_model.findById(user.project, function(err, project) {
                    if(!project.subscribed.includes(
                            vendor.packages[indices[0]]._id))
                    {
                        project.subscribed.push(
                            vendor.packages[indices[0]]._id);
                    } else {
                        vendor.packages[indices[0]].subscribed.splice(index,1);
                    }
                    project.save((err) => {
                        if(err) {
                            console.log(err);
                        }
                        vendor.packages[indices[0]].info.shortlisted++;
                        resolve();
                    });

                })
            });
        } else {
            resolve();
        }
    });
});

/**
 * Automatically duplicate addons for each event based on the package format
 */
vendor_model.createHook(['packages.*.addons','packages.*.format'], (vendor, matchedRule, indices, arrayDiff) => {
    return new Promise(function(resolve, reject) {
        var eventOptions = [
            ['Ceremony'],
            ['Ceremony', 'Reception'],
            ['Reception']
        ];

        let ignore = [];
        let newAddons = [];
        let package = vendor.packages[indices[0]];

        package.addons.reverse().forEach((addon, index) => {
            if(!ignore.includes(addon.optionKey + ',' + addon.optionType)) {
                let repeat = addon.maxAllowed === 0;
                /**
                 * duplicate addons only if the event was newly added or the format
                 * has been changed
                 */
                if(repeat && (addon.selectedEvent == -1 || matchedRule == 1)) {
                    eventOptions[package.format].forEach((e, index2) => {
                        let existing = 0;
                        let exists = package.addons.some((addon2, index3) => {
                            existing = index3;
                            return ((addon2.optionKey == addon.optionKey) &&
                                        (addon2.optionType == addon.optionType) &&
                                        (addon2.selectedEvent == index2));
                        });
                        if(!exists) {
                            let clone = JSON.parse(JSON.stringify(addon));
                            clone.selectedEvent = index2;
                            newAddons.push(clone);
                        } else {
                            if(!newAddons.includes(package.addons[existing])) {
                                newAddons.push(package.addons[existing]);
                            }
                        }
                    });
                    ignore.push(addon.optionKey + ',' + addon.optionType);
                } else {
                    newAddons.push(addon);
                }

            }
        });
        newAddons = newAddons.reverse();
        newAddons.sort((a, b) => (a.optionKey-b.optionKey));
        vendor.packages[indices[0]].addons = newAddons;
        resolve();
    });
})

vendor_model.createHook(['packages.*.published'], function(vendor, matchedRule, indices) {
    return new Promise(function(resolve, reject) {
        let promises = [];
        let imagesGood = true;
        vendor.packages[indices[0]].images.forEach((imageId) => {
            promises.push(new Promise(function(resolve, reject) {
                image_model.findById(imageId, function(err, image) {
                    if (image.sample) {
                        vendor.packages[indices[0]].published = false;
                        imagesGood = false;
                    }
                    resolve();
                });
            }))
        })

        const showMessage = function(messages, message) {
            let found = messages.some((msg) => msg.id == message.id);
            if (!found) {
                messages.push(message);
            }
        }

        const clearMessage = function(messages, id) {
            messages.some((msg, index, array) => {
                if(msg.id == id) {
                    array.splice(index, 1);
                    return true;
                }
            })
        }

        Promise.all(promises).then(() => {
            if (!imagesGood) {
                vendor[symbols.errors]
                    .push('You need to replace all the sample images before publishing!');
            }
            resolve();
        })
    });
});

/**
 * Maintain profile card order
 */
vendor_model.createHook(['profile'], function(vendor, matchedRule, indices, arrayDiff) {
    return new Promise(function(resolve, reject) {
        if(vendor.profile[0].name == 'cover-card' && vendor.profile[1].name == 'seperator-card') {
            resolve();
        } else {
            vendor[symbols.errors]
                .push('Hey, thats not allowed!');
            vendor.profile = vendor[symbols.original].profile;
            resolve();
        }
    });
});

/**
 *  Maintain album likes
 */
vendor_model.createHook(['albums.*.likedBy'], function(vendor, matchedRule, indices) {
    return new Promise(function(resolve, reject) {
        var album = vendor.albums[indices[0]];
        album.likedBy = album.likedBy.filter((value, index, array) => array.indexOf(value) === index);

        // set the liked property to the length of the likedBy array
        album.likes = album.likedBy.length;

        resolve();
    });
});

/**
 *  Maintain album likes
 */
vendor_model.createHook(['packages.*.settings.advanceAmount','packages.*.settings.advancePerc'], function(vendor, matchedRule, indices) {
    return new Promise(function(resolve, reject) {
        let pack = vendor.packages[indices[0]];
        let oldSettings = vendor[symbols.original].packages[indices[0]].settings;
        if(matchedRule == 0) {
            if(pack.settings.advanceAmount <= pack.price.val) {
                pack.settings.advancePerc = pack.settings.advanceAmount/pack.price.val*100;
            } else {
                pack.settings.advanceAmount = oldSettings.advanceAmount;
                vendor[symbols.errors].push('Advance cannot be greater than package cost');
            }
        }
        if(matchedRule == 1) {
            if(pack.settings.advancePerc <= 100) {
                pack.settings.advanceAmount = pack.settings.advancePerc*pack.price.val/100;
            } else {
                pack.settings.advancePerc = oldSettings.advancePerc;
                vendor[symbols.errors].push('Invalid percentage');
            }
        }
        resolve();
    });
});

/**
 *  Maintain default values - for settings
 */
vendor_model.createHook(['packageSettings', 'packages.*.settings'], function(vendor, matchedRule, indices) {
    return new Promise(function(resolve, reject) {
        let defaultKeys = [{
            key: 'advanceDefault',
            pairs: ['advanceRequired','advancePerc']
        }, {
            key: 'balanceDefault',
            pairs: ['balanceDays']
        }, {
            key: 'cancellationDefault',
            pairs: ['refundDeadline', 'refundPerc']
        }];

        vendor.packages.forEach((pack) => {
            defaultKeys.forEach((def) => {
                if(pack.settings[def.key]) {
                    def.pairs.forEach((prop) => {
                        pack.settings[prop] = vendor.packageSettings[prop];
                    });
                }
            });
        });


        resolve();
    });
});

vendor_model.createHook(['packages.*.images'], function(vendor, matchedRule, indices, arrayDiff) {
    return new Promise(function(resolve, reject) {
        console.log(indices[0], arrayDiff);
        resolve();
    });
});
