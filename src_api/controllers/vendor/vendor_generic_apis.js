/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Extract db models from mongoose
 */
var vendor_model = mongoose.model('vendor');

/**
 * Internal tool libraries
 */
var stPeter = require('../../tools/saintPeter');

/**
 * vendorGeneric - expressjs middleware for modifying and/or sending vendor documents
 * to the frontend as per the frontend request using the St.Peter format
 * @param  {Object} req.body.vendorId  vendorId to be modified
 * @param {Object} req.user user document from the jwt token
 * @returns {Array} response array as requested from the frontend
 */
module.exports.vendorGeneric = function(req, res) {
    vendor_model.findById(req.body.vendorId, function(err, vendor) {
        stPeter.checkModifyRespond(vendor, req, res);
    });
}

/**
 * vendorGeneric - expressjs middleware for modifying and/or sending vendor documents
 * to the frontend as per the frontend request using the St.Peter format
 * @param  {Object} req.body.packageId  packageId to be modified
 * @param {Object} req.user user document from the jwt token
 * @returns {Array} response array as requested from the frontend
 */
module.exports.packageGeneric = function(req, res) {
    vendor_model.findOne({'packages._id': req.body.packageId}, function(err, vendor) {
        if(vendor == null) {
            console.log('Generic API: Incorrect packageId');
        }
        var packageIndex = {};
        vendor.packages.forEach((package, index) => {
            if(package._id.toString() == req.body.packageId) {
                packageIndex = index;
            }
        });
        var rootPath = 'packages.' + packageIndex + '.';
        stPeter.checkModifyRespond(vendor, req, res, rootPath);
    });
}
