/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Referenced data files
 */
var serviceData = require('../../data/serviceData');
var vendorData = require('../../data/vendorData');

/**
 * Extract db models from mongoose
 */
var vendor_model = mongoose.model('vendor');
var project_model = mongoose.model('project');
var image_model = mongoose.model('image');


// START - UTILITY FUNCTIONS

/**
 * initVendor - this function is invoked by the authentication API to create
 * a new Vendor object
 * @returns {Object Id} the ID of the created object
 */
module.exports.initVendor = function(userId) {
    // create new vendor object
    var vendor = new vendor_model();
    // populate the profile with the default values
    vendor.profile = vendorData.defaults.profile;

    vendor.staff.push(userId);

    // manually marking as modified to trigger the hooks
    // the actual values will be set through the defaults in the schema
    vendor.markModified('vendorType');
    vendor.markModified('name');

    image_model.aggregate([
        {$match: {
            'sample': true
        }}
    ]).exec(function(err, images) {

        if(images.length == 0) {
            throw "No images found in the database, try restarting the server!";
        }

        var imageIndex = Math.floor(Math.random()*images.length);

        vendor.dp = images[imageIndex];
        vendor.cover = images[imageIndex+1];

        // save new vendor object
        vendor.save(function(err) {
            if(err) {
                console.log(err);
            }
        });
    });

    return vendor._id;
};

// END - UTILITY FUNCTIONS
