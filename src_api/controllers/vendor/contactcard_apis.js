/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Other referenced controllers
 */
var vendorUtility = require('./vendor_utility');

/**
 * Referenced data files
 */
var serviceData = require('../../data/serviceData');
var vendorData = require('../../data/vendorData');

/**
 * Extract db models from mongoose
 */
var user_model = mongoose.model('user');
var vendor_model = mongoose.model('vendor');
var project_model = mongoose.model('project');
var image_model = mongoose.model('image');


/**
 * NO CONTACT CARD SPECIFIC APIS YET, ALL ACTIONS ARE COMPLETED THROUGH THE GENERIC API
 */
