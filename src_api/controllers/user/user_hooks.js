/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Extract db models from mongoose
 */
var user_model = mongoose.model('user');
var vendor_model = mongoose.model('vendor');

/**
 * Reference DB Controllers
 */
var vendorDB = require('../../models/vendor');

/**
 * User title update hook
 *
 * this hook listens to changes in the vendor name and update the user title
 * accordingly.
 *
 * NOTE: This hook was implemented largely to test the hooks on the vendor
 * collection. actual functionality probably should be moved elsewhere
 *
 * @param  {String Array} ['name']          watch for changes in 'name' property
 * @param  {function} function      callback to be executed on change detection
 * @return {Promise} Promise        promise returned
 */
vendor_model.createHook(['name'], function(vendor, matchedRule, indices) {
    return new Promise(function(resolve, reject) {
        var promises = [];
        /**
         * If vendor name changed modify the title of each staff member
         * accordingly.
         * Title is what's shown below the user name in the navigation pane
         */
        vendor.staff.forEach((staffId) => {
            promises.push(new Promise(function(resolveP, rejectP) {
                user_model.findById(staffId, function(err, user) {
                    if(user) {
                        user.title = vendor.name;
                        user.save(function(err) {
                            if(err) {
                                console.log(err);
                            } else {
                                resolveP();
                            }
                        })
                    } else {
                        resolveP();
                    }
                });
            }));
        });
        Promise.all(promises).then(function() {
            resolve();
        })
    });
});
