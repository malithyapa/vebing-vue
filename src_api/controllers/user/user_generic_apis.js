/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Extract db models from mongoose
 */
var user_model = mongoose.model('user');

/**
 * Internal tool libraries
 */
var stPeter = require('../../tools/saintPeter');

/**
* module.exports.login - expressjs middleware function for handing  local user
* login
* @param {Object} req.user user document from the jwt token
* @returns {Object} navigation elements for this user
*/
module.exports.getElements = function(req, res) {
    res.status(200).json({
       elements: req.user.elements
    })
};

/**
 * vendorGeneric - expressjs middleware for modifying and/or sending vendor documents
 * to the frontend as per the frontend request using the St.Peter format
 *
 * If userId is nor specified in the request object the method will fallback
 * to the userId from the JWT token
 *
 * @param  {Object} req.body.userId  vendorId to be modified
 * @param {Object} req.user user document from the jwt token
 * @returns {Array} response array as requested from the frontend
 */
module.exports.userGeneric = function(req, res) {
    stPeter.checkModifyRespond(req.user, req, res);
}
