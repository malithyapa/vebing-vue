/**
* Module dependencies.
*/
var passport = require('passport');
var mongoose = require('mongoose');
var socket = require('../socket');
var uuid = require('uuid/v4');
var mail = require('../email');

/**
* Other referenced controllers
*/
var vendorUtility = require('../vendor/vendor_utility');
var plannerUtility = require('../planner/planner_utility');
var calendarUtility = require('../calendar/calendar_utility');

/**
* Referenced data files
*/
var vendorData = require('../../data/vendorData');
var plannerData = require('../../data/plannerData');

/**
* Extract db models from mongoose
*/
var user_model = mongoose.model('user');

/**
 * Register user hooks
 */
var userHooks = require('./user_hooks');

/**
* register - expressjs middleware function for handing  local user
* registrations
* @param {String} req.body.name name of user
* @param {String} req.body.email email
* @param {String} req.body.password password
* @param {String} req.body.gender user gender
* @returns {String} the error or status message
*/
module.exports.register = function(req, res) {

    // should discuss if gender is neccesary

    // check if the request contains all the required information
    if(!req.body.name || !req.body.email || !req.body.password || !req.body.gender  ) {
        res.status(400).json({
            'error': 'Hey, we need you to fill all the fields!'
        });
        return;
    }

    // check if the user already exists
    user_model.findOne({email: req.body.email}, function(err, user) {
        if(!user) {
            // create a new user object if user is not found
            user = new user_model();
        } else {
            if(user.auth) {
                if(user.auth == 'activated') {
                    res.status(400).json({
                        'error': "This email is already registered. Forgot your password?"
                    });
                    return;
                } else {
                    res.status(400).json({
                        'error': "You've already signed up. Check your inbox for the activation link!"
                    });
                    return;
                }
            }
        }

        // populate the new user object
        user.name = req.body.name;
        user.email = req.body.email;
        user.gender = req.body.gender;

        // set authentication type as local
        user.authType = "local"

        // ser the user password (hash and salt) using the mongoose schema method
        user.setPassword(req.body.password);

        // check if the new user is a vendor or a user
        if(req.body.vendor) {
            // populate the initial navigator options for the user using the
            // defaults for a vendor
            user.elements = vendorData.defaults.elements;

            //temp user title until wedding name is implemented
            user.title = "Your company name";

            //temporary user keys for editing packages
            user.permissionKeys.push(0);

            // initialize a new vendor object and assign it to the user
            user.vendor = vendorUtility.initVendor(user._id);
        } else {
            // populate the initial navigator options for the user using the
            // defaults for a planner
            user.elements = plannerData.defaults.elements;

            //temp user title until wedding name is implemented
            user.title = "My wedding"

            // initialize a new project and assign it to the user
            user.project = plannerUtility.initProject();
        }

        // user.auth = uuid();
        user.auth = 'activated'; // temporarily disabling link authentication

        // send the activation link to the user
        mail.activationEmail(user.email,user.auth);

        // assign calendar
        user.calendar = calendarUtility.initCalendar(user._id);

        // save the new user object
        user.save(function(err) {
            if(err) {
                console.log('couldnt save!');
            }
            res.status(200).json({'error': 'Please check your email for the verification.'});
        });
    });
};

/**
* login - expressjs middleware function for handing  local user login
* @param {String} req.body.email user email
* @param {String} req.body.password user password
* @returns {Object} status message and jwt token if successful
*/
module.exports.login = function(req, res) {

    // check if the request contains all the required information
    if(!req.body.email || !req.body.password) {
        sendJSONresponse(res, 400, {
            "error": "Hey, we need you to fill in all the fields!"
        });
        return;
    }

    // authenticate the user using the passport local strategy
    passport.authenticate('local', function(err, user, info) {
        var token;

        // If Passport throws/catches an error
        if(err) {
            res.status(400).json(err.toString());
            return;
        }
        if(!user) {
            res.status(400).json({'error': "Wrong username or password!"});
            return;
        }
        if(user.auth != 'activated') {
            res.status(400).json({'error': "User email not verified. Find the verification link in your inbox!"});
            return;
        }

        token = user.generateJwt();

        res.status(200).json({
            "token" : token,
            "namespaces" : user.namespaces
        });

    })(req, res);
};

/**
* activate - expressjs middleware function for handing  local user
* activation
* @param {String} req.query.id Authentication id embedded in the request URL
* @returns {String} redirect url for the frontend
*/
module.exports.activate = function(req, res) {
    user_model.findOne({auth: req.query.id}, function(err, user) {
        if(!err) {
            if(user) {
                user.auth = 'activated';
                // user.rooms.push({name: user._id.toString() + '-system',seen: 0});
                user.save();
                // redirect frontend to indicate email has been validated
                res.redirect('/#/login/authenticated');
                res.status(200);
            } else {
                console.log('user not found - activate');
            }
        }
    });
};

/**
 * module.exports.facebook - expressjs middleware function for handing  facebook user login and registration
 * login
 * @param {String} req.user.access_token user access token from passport
 * @param {String} req.user.profile user profile from the passport
 * @returns {Object} status message and jwt token if successful
 */
module.exports.facebook = function(req, res) {
    user_model.findOne({email: req.body.email}, function(err, user) {
        if(!user) {
            // check if the request contains all the required information
            if(!req.body.name || !req.body.email || !req.body.gender  ) {
                res.status(400).json({
                    'error': 'Facebook login failed! Please register using email and password'
                });
                return;
            }

            // create a new user object if user is not found
            user = new user_model();

            // populate the new user object
            user.name = req.body.name;
            user.email = req.body.email;
            user.gender = req.body.gender;

            // set authentication type as facebook
            user.authType = "facebook"

            // check if the new user is a vendor or a user
            if(req.body.vendor) {
                // populate the initial navigator options for the user using the
                // defaults for a vendor
                user.elements = vendorData.defaults.elements;

                //temp user title until wedding name is implemented
                user.title = "Your company name";

                //temporary user keys for editing packages
                user.permissionKeys.push(0);

                // initialize a new vendor object and assign it to the user
                user.vendor = vendorUtility.initVendor(user._id);
            } else {
                // populate the initial navigator options for the user using the
                // defaults for a planner
                user.elements = plannerData.defaults.elements;

                //temp user title until wedding name is implemented
                user.title = "My wedding"

                // initialize a new project and assign it to the user
                user.project = plannerUtility.initProject();
            }

            // facebook authentication requires no secondary activation
            user.auth = 'activated';

            // assign calendar
            user.calendar = calendarUtility.initCalendar(user._id);

            // save the new user object
            user.save(function(err) {
                if(err) {
                    console.log('couldnt save!');
                }
                var token = user.generateJwt();

                res.status(200).json({
                    "token" : token,
                    "namespaces" : user.namespaces
                });
            });
        } else {
            // authenticate the user using the passport jwt strategy
            var token = user.generateJwt();

            res.status(200).json({
                "token" : token,
                "namespaces" : user.namespaces
            });

        }
    });
};
