/**
 * External libraries
 */
var socketioJwt = require('socketio-jwt');

module.exports.socket = null;

module.exports.setSocket = function(socket) {
    module.exports.socket = socket;


    module.exports.socket.on('connection', socketioJwt.authorize({
        secret: 'MY_SECRET',
        timeout: 15000 // 15 seconds to send the authentication message
    })).on('authenticated', function(socket) {
        // this socket is authenticated, we are good to handle more events from it.
        socket.join(socket.decoded_token._id);

        socket.on('server-console', (data) => {
            console.log(data);
        })
    });
};
