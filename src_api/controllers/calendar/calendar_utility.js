/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Extract db models from mongoose
 */
var user_model = mongoose.model('user');
var calendar_model = mongoose.model('calendar');

// START - UTILITY FUNCTIONS

/**
 * initCalendar - express js middleware for initializing a calendar document for a new user
 * only retrieves the data for a vendor of his own calendar for a the time period X - to be determined
 * @param  {String} req.payload._id
 * @returns {Object} data to be displayed
 */
module.exports.initCalendar = function(userId) {
    var calendar = new calendar_model();
    calendar.user = userId;

    calendar.save(function(err) {
        if(err)
        console.log(err);
    });

    return calendar._id;
};
