/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Extract db models from mongoose
 */
var user_model = mongoose.model('user');
var calendar_model = mongoose.model('calendar');

/**
 * Event sync hook - edit event
 *
 * this hook listens to changes in the calendar events sync multiuser events between users
 *
 * @param  {String Array} ['events']          watch for changes in 'name' property
 * @param  {function} function      callback to be executed on change detection
 * @return {Promise} Promise        promise returned
 */
calendar_model.createHook(['events','events.*.users','events.*','events.*.title'], (calendar, matchedRule, indices, arrayDiff) => {
    return new Promise(function(resolve, reject) {
        var promises = [];
        /**
         * Rule 0 - New Event Addition
         * If the event contains multiple users a copy of the event will be added to each users calendar
         */
        if (matchedRule == 0 && indices == undefined && arrayDiff.added[0] != undefined && arrayDiff.removed[0] == undefined) {
            // console.log("Event Add");
            // console.log("createCalendarHook","\n\tRule: ", matchedRule,"\n\tindices: ", indices,"\n\tindices type: ", typeof(indices),"\n\tarrayDiff: ", arrayDiff);
            var event = calendar.events[arrayDiff.added[0]];
            pUserIds = event.users;
            if (event.owner.equals(calendar.user)){
                if (pUserIds.length > 1){
                    pUserIds.forEach((pUserId) => {
                        user_model.findById(pUserId, function(err, pUser) {
                            if(err) {
                                console.log("createCalendarHook",matchedRule,": No user found: ", pUser._id, err);
                            }
                            if (!pUserId.equals(calendar.user)){
                                calendar_model.findOneAndUpdate({"_id": pUser.calendar}, {"$push": {"events": event}}, function(err, pUserCalendar) {
                                    if(err) {
                                        console.log("createCalendarHook",matchedRule,": Error adding new event to user: ", pUser._id, err);
                                    }
                                });
                            }
                        });
                    })
                }
            }
        }
        /**
         * Rule 0 - New Event Removal
         * If the event contains multiple users
         * If the owner perfromed the deletion: copy of the event will be removed from each users calendar
         * Else  copy of the event will not be removed from each users calendar
         */
        if (matchedRule == 0 && indices == undefined && arrayDiff.added[0] == undefined && arrayDiff.removed[0] != undefined) {
            // console.log("Event Remove",);
            // console.log("createCalendarHook","\n\tRule: ", matchedRule,"\n\tindices: ", indices,"\n\tindices type: ", typeof(indices),"\n\tarrayDiff: ", arrayDiff);    
        }
        /**
         * Rule 1 - Event user list modify
         * If new user added - add a copy of the event to new user
         * If an user removed - remove copy of event from user
         */
        if (matchedRule == 1) {
            // console.log("Event user list modify");
            // console.log("createCalendarHook","\n\tRule: ", matchedRule,"\n\tindices: ", indices,"\n\tindices type: ", typeof(indices),"\n\tarrayDiff: ", arrayDiff);
            var event = calendar.events[indices];
            pUserIds = event.users;
            if (event.owner.equals(calendar.user)){
                // remove user
                if (arrayDiff.removed.length != 0){
                    // update existing users
                    pUserIds.forEach((pUserId) => {
                        if (!pUserId.equals(calendar.user)){
                            user_model.findById(pUserId, function(err, pUser) {
                                if(err) {
                                    console.log("createCalendarHook",matchedRule,": No user found: ", pUser._id, err);
                                }
                                calendar_model.findOneAndUpdate({"_id": pUser.calendar, "events._id": event._id}, {"$set": {"events.$": event}}, {upsert: true}, function(err2, pCalendar) {
                                    if(err2) {
                                        // console.log("createCalendarHook",matchedRule,": No event found: ", pUser._id, err2);
                                    }
                                    if(pCalendar == null) {
                                        calendar_model.findOneAndUpdate({"_id": pUser.calendar}, {"$push": {"events": event}}, function(err3, pCalendar2) {
                                            if(err3) {
                                                console.log("createCalendarHook",matchedRule,": Error updating/adding existing user: ", pUser._id, err3);
                                            }
                                        });
                                    }
                                });
                            });
                        }
                    });
                    // delete event from removed user(s)
                    arrayDiff.removed.forEach((index) => {
                        user_model.findById(index, function(err, pUser) {
                            if(err) {
                                console.log("createCalendarHook",matchedRule,": No user found: ", err);
                            }
                            calendar_model.findOneAndUpdate({"_id": pUser.calendar, "events._id": event._id}, {"$pull": {"events": { "_id": event._id }}}, function(err2, pCalendar) {
                                if(err2) {
                                    console.log("createCalendarHook",matchedRule,": Event not found: ", err2);
                                }
                            });
                        });
                    });
                }
                // add user
                else if (arrayDiff.added.length != 0){
                    pUserIds.forEach((pUserId) => {
                        // console.log("User added ", pUserIds[index]);
                        if (!pUserId.equals(calendar.user)){
                            user_model.findById(pUserId, function(err, pUser) {
                                if(err) {
                                    console.log("createCalendarHook",matchedRule,": No user found: ", pUser._id, err);
                                }
                                calendar_model.findOneAndUpdate({"_id": pUser.calendar, "events._id": event._id}, {"$set": {"events.$": event}}, {upsert: true}, function(err2, pCalendar) {
                                    if(err2) {
                                        // console.log("createCalendarHook",matchedRule,": No event found: ", pUser._id, err2);
                                    }
                                    if(pCalendar == null) {
                                        calendar_model.findOneAndUpdate({"_id": pUser.calendar}, {"$push": {"events": event}}, function(err3, pCalendar2) {
                                            if(err3) {
                                                console.log("createCalendarHook",matchedRule,": Error updating/adding new user: ", pUser._id, err3);
                                            }
                                        });
                                    }
                                });
                            });
                        }
                    });
                }
            }
        }
        /**
         * Rule 2 - Event property modify
         * If the event contains multiple users copy of the event will be removed from each users calendar
         */
        if (matchedRule == 2 && indices != undefined) {
            // console.log("Event property modify");
            // console.log("createCalendarHook","\n\tRule: ", matchedRule,"\n\tindices: ", indices,"\n\tindices type: ", typeof(indices),"\n\tarrayDiff: ", arrayDiff);
            event = calendar.events[indices];
            pUserIds = event.users;
            if (event.owner.equals(calendar.user)){
                if (pUserIds.length > 1){
                    pUserIds.forEach((pUserId) => {
                        user_model.findById(pUserId, function(err, pUser) {
                            if(err) {
                                console.log("createCalendarHook",matchedRule,": No user found: ", pUser._id, err);
                            }
                            if (!pUserId.equals(calendar.user)){
                                calendar_model.findOneAndUpdate({"_id": pUser.calendar, "events._id": event._id}, {"$set": {"events.$": event}}, function(err, pUserCalendar) {
                                    if(err) {
                                        console.log("createCalendarHook",matchedRule,": Error updating event in user: ", pUser._id, err);
                                    }
                                });
                            }
                        });
                    })
                }
            }
        }

        /**
         * Rule 3 - Specific event property edit
         * If the event contains multiple users copy of the event will be removed from each users calendar
         */
        if (matchedRule == 3 ) {
            // console.log("Specific event property edit");
            //console.log("createCalendarHook","\n\tRule: ", matchedRule,"\n\tindices: ", indices,"\n\tindices type: ", typeof(indices),"\n\tarrayDiff: ", arrayDiff);
        }
        resolve();
    });
});
