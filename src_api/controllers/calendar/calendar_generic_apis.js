/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Extract db models from mongoose
 */
var calendar_model = mongoose.model('calendar');

/**
 * Register calendar hooks and utility
 */
// var calendarHooks = require('./calendar_hooks_old');
var calendarHooks = require('./calendar_hooks');

/**
 * Internal tool libraries
 */
var stPeter = require('../../tools/saintPeter');

/**
 * calendarGeneric - expressjs middleware for modifying and/or sending calendar documents
 * to the frontend as per the frontend request using the St.Peter format
 *
 * @param  {Object} req.body.calendarId  calendarId to be modified
 * @returns {Array} response array as requested from the frontend
 */
module.exports.calendarGeneric = function(req, res) {
    calendar_model.findById(req.body.calendarId, function(err, calendar) {
        stPeter.checkModifyRespond(calendar, req, res);
    });
}
