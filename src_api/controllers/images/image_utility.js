/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Extract db models from mongoose
 */
var image_model = mongoose.model('image');

/**
 * Reference DB Controllers
 */
var imageDB = require('../../models/images');

/**
 * External libraries
 */
var fs = require('fs');

/**
 * This function runs on server startup and populates the database with all the
 * images in the ./sample directory if it hasn't already been added.
 *
 * This function ensures that there will always be sample images to be used in
 * sample packages and elsewhere even after a database reset
 */
fs.readdir('./sample', (err, files) => {
    files.forEach(file => {
        image_model.find({src: file}, function(err, image) {
            if(!image.length) {
                var image = new image_model();
                image.src = file;
                image.location = 'This is a sample file';
                image.caption= 'Caption Here';
                image.sample = true;
                image.save(function(err) {
                    if(err) {
                        console.log(err);
                    }
                });
            }
        });
    });
})
