/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Extract db models from mongoose
 */
var image_model = mongoose.model('image');

/**
 * Image likes hook
 *
 * this hook listens to changes in the likedBy array and sets the length of the
 * array as the likes property
 *
 * NOTE: It is possible to implement this as a virtual property in the schema
 * aswell. Should be considered as an alternative
 *
 * @param  {String Array} ['likedBy']          watch for changes in 'likedBy' property
 * @param  {function} function      callback to be executed on change detection
 * @return {Promise} Promise        promise returned
 */
image_model.createHook(['likedBy'], function(image, matchedRule, indices) {
    return new Promise(function(resolve, reject) {

        /**
         * Check and remove any duplicate values in the likedBy array
         * While the frontend won't let a user like an image if it's already
         * liked, this ensures that liked can't be duped even if the frontend
         * checks were somehow bypassed
         */
        image.likedBy = image.likedBy.filter((value, index, array) => array.indexOf(value) === index);

        // set the liked property to the length of the likedBy array
        image.likes = image.likedBy.length;

        resolve();
    });
});
