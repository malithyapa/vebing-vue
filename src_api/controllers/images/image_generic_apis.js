/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var sharp = require('sharp');
var fs = require('fs');
var path = require('path');

/**
 * Extract db models from mongoose
 */
 var user_model = mongoose.model('user');
 var vendor_model = mongoose.model('vendor');
 var project_model = mongoose.model('project');
 var image_model = mongoose.model('image');

/**
 * Register image hooks and utility
 */
var imageHooks = require('./image_hooks');
var imageUtility = require('./image_utility');

/**
 * Internal tool libraries
 */
var stPeter = require('../../tools/saintPeter');

/**
 * saveAndResizeImage - expressjs middleware for resizing uplodaed images to
 * specific predefined formats. every image is resized to 150x150, 250x250 and
 * 420x250
 * @param {Object} req.files.photos  photos object added from the formidable firmware
 * @param {Object} req.user user document from the jwt token
 * @augments res.image image object information to be passed to the next() middleware
 * @returns {String} name of the uploaded image
 */
module.exports.saveAndResizeImage = function(req, res, next) {

    var name = req.files.photos.path.split('/')[1];

    // sharp pipeline for resizing images
    sharp(req.files.photos.path)
        // size to be used for the display picture
        .resize(150, 150)
        .toFile(path.join('./uploads/150x150', name), function(err, info) {
            if(err)
                console.log(err);
        })
        .resize(250,250)
        .toFile(path.join('./uploads/250x250',name), function(err, info) {
            if(err)
                console.log(err);
        })
        // size to be used for the cover picture
        .resize(420, 250)
        .toFile(path.join('./uploads/420', name), function(err, info) {
            if(err)
                console.log(err);
        });

    // create a new image object and pass it's id and name to the next()
    // middleware
    var image = new image_model();
    image.src = name;
    image.by = req.user._id;
    image.vendor = req.user.vendor;
    image.likes = 0;
    image.comments = [];
    image.location = 'Add Location';
    image.save(function(err) {
        if(err)
            console.log(err);
        res.image = {
            _id: image._id,
            name: name
        }
        res.status(200).json({id: image._id});
        next();
    });
};

/**
 * imageGeneric - expressjs middleware for modifying and/or sending image documents
 * to the frontend as per the frontend request using the St.Peter format
 * @param {Object} req.body.imageId  imageId to be modified
 * @param {Object} req.user user document from the jwt token
 * @returns {Array} response array as requested from the frontend
 */
module.exports.imageGeneric = function(req, res) {
    image_model.findById(req.body.imageId, function(err, image) {
        stPeter.checkModifyRespond(image, req, res);
    });
}
