/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/*
 * Import momentjs for time value manipulation
 */
var moment = require('moment');

/**
 * Global symbols
 */
const symbols = require('./globalSymbols');

function addAuthError(err, user) {
    console.log(err);
    if(!user.ignore) {
        user.authErr.push(err)
    }
}

function processPath(path, modifyDocument) {

    let parentSearch = path.match(/([\.])\1+/);

    if(parentSearch) {
        let backCount = parentSearch[0].length - 1;
        let root = path.substring(0, parentSearch.index);
        let subPath = path.substring(parentSearch.index + backCount + 1)
        let newPath = ''
        root.split('.').forEach((elem, index, arr) => {
            if(index<(arr.length-backCount)) {
                if(newPath != '') {
                    newPath += '.'
                }
                newPath += elem;
            }
        });
        if(newPath != '') {
            newPath += '.'
        }
        newPath += subPath;
        path = newPath;
    }

    let negIndex = path.match(/-\d+/g);

    if(negIndex != null) {
        negIndex.forEach((index) => {
            let split = path.split(index);
            let arrayPath = split[0].substring(0, split[0].length - 1);
            let arrayLength = modifyDocument.get(arrayPath).length;
            let newIndex = arrayLength + parseInt(index);
            path = split[0] + newIndex + split[1];
        })
    }

    return path;
}

/**
 * getPermission - decodes the permission object and returns a simple true
 * or false value for whether the current user has write permissions as per
 * the passed permission object and the required operation
 *
 * @param  {Object} permissions The permissions object from the db schema
 * @return {Boolean} read permission
 */
function getPermissions(permissions, modifyDocument, path) {

    let opsArray = ['modify', 'add', 'remove', 'read'];

    let userPermits = {};

    opsArray.forEach((opPerm, index) => {

        userPermits[opPerm] =  false;

        let authorizedUsers = [];
        let requiredKeys = [];

        // find all authorized users as per permissions object
        permissions[opPerm].forEach((perm) => {
            if(/^\./.test(perm)) {
                let backspaces = perm.split('.').length-2;
                let tempPath = path;
                if(backspaces > 0) {
                    let indices = [];
                    let startIndex = path.indexOf('.');
                    while(startIndex >= 0) {
                        indices.push(startIndex);
                        startIndex = path.indexOf('.', startIndex+1);
                    }
                    tempPath = path.substring(0,
                            indices[indices.length-backspaces]);
                    perm = perm.substring(backspaces)
                }
                perm = tempPath + perm;
            }
            if(perm == 'any') {
                authorizedUsers = ['any']
            } else if(isNaN(perm)) {
                if(modifyDocument.get(perm)) {
                    authorizedUsers = authorizedUsers
                        .concat(modifyDocument.get(perm));
                }
            } else {
                requiredKeys.push(parseInt(perm));
            }
        });

        // check if the authorized users have the required permission keys
        authorizedUsers.forEach((aUser) => {
            if((aUser.toString() == user._id.toString()) || aUser == 'any') {
                var keyFound = true;
                requiredKeys.forEach((rKey) => {
                    if(!user.permissionKeys.includes(rKey)) {
                        keyFound = false;
                    }
                });
                if(keyFound) {
                    userPermits[opPerm] =  true;
                }
            }
        });

    })

    return userPermits
}

/**
 * descendSchema - checks if the schema has a child schema object
 * for the specified key.
 * If a child schema object is found, returns a reference to the child
 * schema
 * If a child schema object is not found, returns the same schema object
 * If the child schema, does not have a readPerm value set the readPerm
 * value is either inferred from the permissions object if present, or
 * from the parent schema (inheritance)
 *
 * @param  {string} key         property name to look for permissions
 * @param  {Object} localSchema schema context which has the above property
 * @return {Object}             either the same schema or the child schema
 * object if present
 */
function descendSchema(key, localSchema, path, modifyDocument) {


    let tempLocal = localSchema;
    let schemaFound = false;
    let permissionsKey = key;

    if(!isNaN(key)) {
        if(key > 0) {
            if(localSchema[0]) {
                localSchema[key] = JSON.parse(JSON.stringify(localSchema[0]))
            }
        }
    }

    if(typeof localSchema[key] == 'object' && localSchema[key]) {
        schemaFound = true;
        localSchema = localSchema[key];
        if(localSchema.obj) {
            localSchema = localSchema.obj;
        }
    }

    if(localSchema instanceof Array && localSchema[0] != null) {
        if(localSchema[0].obj) {
            if(localSchema[0].obj.permissions && schemaFound) {
                localSchema.userPermits = getPermissions(localSchema[0].obj.permissions,
                                            modifyDocument, path + '.0');
                localSchema.userPermits.read = true;
            }
        }
    }

    if(localSchema.permissions && schemaFound) {
        localSchema.userPermits = getPermissions(localSchema.permissions, modifyDocument,
            path);
    }

    if(localSchema.userPermits == null) {
        localSchema.userPermits = tempLocal.userPermits;
    }

    return localSchema;
}

module.exports.checkAndRead = function(originPath, modifyDocument, user) {

    // modify path to handle negative indices
    originPath = processPath(originPath, modifyDocument);

    // the schema object is cloned to make sure that the readPerm value is
    // not cached
    var schema = JSON.parse(JSON.stringify(modifyDocument.schema.obj));

    // check for root level permissions object
    if(!schema.permissions) {
        console.log('St.Peter : Root level permission missing in schema');
        return null;
    }

    // set root level read permissions
    schema.userPermits = getPermissions(schema.permissions, modifyDocument, originPath);

    /**
     * 1) Decode the path specified in the 'get' object and set the readObject
     * to the value that is requested.
     *
     * Note: 'unwind' and 'all' are special paths which correlate to the unwounded
     * document and the complete document respectively.
     * Refer mongodb $unwind operator for an explanation.
     *
     * Note: if the path is 'unwind' a special property unwindIndex is set on the
     * parent object to indicate the indicate the index of the array that was
     * unwound
     *
     * 2) Set the originPath to represent the starting point for scanning the
     * schema for permissions
     *
     */
    var readObject = {};

    if(RegExp('unwind$').test(originPath)) {
        if(originPath == 'unwind') {
            console.log('St.Peter : Invalid use of unwound');
            return null;
        } else {
            let subPath = originPath.replace(/.unwind/,'');
            let tempObject = modifyDocument.get(subPath).toObject({virtuals: true});
            let unwindIndex = subPath.match(/\d$/)[0];
            subPath = subPath.replace(/.\d$/,'');
            let subPathKeys = subPath.split('.');

            readObject = modifyDocument.toObject({virtuals: true});

            let objectRef = readObject;
            let keyRef = '';
            subPathKeys.forEach((key, index) => {
                if(index != (subPathKeys.length - 1)) {
                    objectRef = objectRef[key];
                }
                keyRef = key;
            });

            objectRef[keyRef] = tempObject;
            objectRef[keyRef].unwounded = true;
            objectRef.unwindIndex = unwindIndex;

            originPath = '';

        }

    } else if(RegExp('all$').test(originPath)) {

        if(originPath == 'all') {
            originPath = '';
            readObject = modifyDocument;
        } else {
            originPath = originPath.replace('.all','');
            readObject = modifyDocument.get(originPath);
        }

    } else {
        readObject = modifyDocument.get(originPath);
    }

    /**
     * if readObject is null print invalid get path on the console
     */
    if(readObject == null) {
        console.log('St.Peter : Invalid get path,', originPath);
        return null;
    }

    /**
     * bring schema into the context of the requested property
     */
    var path = '';
    originPath.split('.').forEach((key) => {
        path = (path == '') ? key : (path + '.' + key);
        schema = descendSchema(key, schema,
            path, modifyDocument);
    });

    /**
     * convert readObject to a standard javascript object from a mongoose
     * document
     */
    if(typeof readObject.toObject == 'function') {
        readObject = readObject.toObject({virtuals: true});
    }

    /**
     * scanObject - scan each key of the object to see if the schema
     * contains a readPerm value true
     * for each new key descendPermissions is called to see if the schema has
     * a child schema specified
     * if the key represents an object scanObject is called recursively with the
     * child schema
     * if readPerm is false the respective value for that key is set to null
     *
     * @param  {Object} object      object to scan
     * @param  {Object} localSchema schema in the same context as the above object
     * @return {Object}             object with unauthorized values removed
     */
    const scanObject = function(object, localSchema, path) {
        if(localSchema.userPermits.read) {
            Object.getOwnPropertyNames(object).forEach((prop) => {
                let newPath = ((path != '') ? (path + '.') : '') + prop;
                if(typeof object[prop] == 'object' && object[prop]) {
                    let propSchema = descendSchema(prop, localSchema,
                        newPath, modifyDocument);
                    // descend the schema into the array for unwounded objects
                    if(object[prop].unwounded) {
                        newPath = newPath + '.' + object.unwindIndex;
                        propSchema = descendSchema(object.unwindIndex, propSchema,
                            newPath, modifyDocument);
                    }
                    if(!/id$/.test(prop) && !/^_/.test(prop)) {
                        object[prop] = scanObject(object[prop], propSchema, newPath);
                    } else {
                        return object[prop];
                    }
                } else {
                    let propSchema = descendSchema(prop, localSchema,
                        newPath, modifyDocument);
                    if(!propSchema.userPermits.read) {
                        addAuthError('St.Peter : Unauthorized read blocked, ' +
                            newPath, user);
                        return null;
                    } else {
                        return object[prop];
                    }
                }
            });
            return object;
        } else {
            addAuthError('St.Peter : Unauthorized read blocked, ' + path, user);
            return null;
        }
    }

    return scanObject(readObject, schema, originPath);
}

module.exports.checkAndWrite = function(originPath, modifyDocument, user, value) {

    // modify path to handle negative indices
    originPath = processPath(originPath, modifyDocument);

    // decode the operation and the relevant permission sub-object
    var op = 0;
    var opPerm = 'modify';

    if(/\+$/.test(originPath)) { // push to array
        op = 1;
        opPerm = 'add';
    } else if(/\-$/.test(originPath)) { // remove from array with index
        op = 2;
        opPerm = 'remove';
    } else if(/\^$/.test(originPath)) { // remove from array with element
        op = 3;
        opPerm = 'remove';
    } else if(/\>$/.test(originPath)) { // move element
        op = 4;
        opPerm = 'modify';
    } else if(/\*$/.test(originPath)) { // remove element with _id
        op = 5;
        opPerm = 'remove';
    }

    originPath = originPath.replace(/\+|\-|\^|\>|\*/, '');

    // the schema object is cloned to make sure that the writePerm value is
    // not cached
    var schema = JSON.parse(JSON.stringify(modifyDocument.schema.obj));

    // check for root level permissions object
    if(!schema.permissions) {
        console.log('St.Peter : Root level permission missing');
        return null;
    }

    // set root level write permissions
    schema.userPermits = getPermissions(schema.permissions, modifyDocument, originPath);

    /**
     * bring schema into the context of the requested property
     */
    var path = '';
    originPath.split('.').forEach((key) => {
        path = (path == '') ? key : (path + '.' + key);
        schema = descendSchema(key, schema, path, modifyDocument);
    });

    /**
     * Recursively scan values for special key words and replace if any found
     */
    const passValue = function(value) {
        if (typeof value == 'object') {
            Object.getOwnPropertyNames(value).forEach((prop) => {
                if (value[prop]) {
                    value[prop] = passValue(value[prop]);
                }
            });
            return value;
        } else {
            if (typeof value == 'string') {
                if (value == '$user') {
                    value = new mongoose.Types.ObjectId(user._id);
                }
            }
            return value;
        }
    }

    /**
     * recursively scan permissions for modify operations
     */
    if(op === 0) {

        /**
         * Thig flag will be set to true if any of the child properties of the
         * object modified lack the required permissions.
         */
        let flagged = false;

        /**
         * scanObject - scan each key of the object to see if the schema
         * contains a writePerm value true
         * for each new key descendPermissions is called to see if the schema has
         * a child schema specified
         * if the key represents an object scanObject is called recursively with
         * the child schema
         * if writePerm is true then that value is modified
         *
         * @param  {Object} object      object to scan
         * @param  {Object} localSchema schema in the same context as the above object
         * @return {Object}             object with unauthorized values removed
         */
        const scanObject = function(object, localSchema, path, localValue) {
            if(localSchema.userPermits[opPerm]) {
                Object.getOwnPropertyNames(object).forEach((prop) => {
                    let newPath = ((path != '') ? (path + '.') : '') + prop;
                    if(localValue[prop]) {
                        if(typeof object[prop] == 'object' && object[prop]) {
                            let propSchema = descendSchema(prop, localSchema,
                                newPath, modifyDocument);
                            if(localValue[prop] instanceof Array) {
                                if(!(propSchema.userPermits.add &&
                                    propSchema.userPermits.remove)) {
                                        flagged = true;
                                        localValue[prop] = object[prop];
                                        addAuthError('St.Peter : Unauthorized array replacement, ' +
                                            newPath, user);
                                } else {
                                    modifyDocument.markModified(newPath);
                                }
                            } else {
                                scanObject(object[prop], propSchema, newPath,
                                    localValue[prop]);
                            }
                        } else {
                            let propSchema = descendSchema(prop, localSchema,
                                newPath, modifyDocument);
                            if(!propSchema.userPermits[opPerm]) {
                                flagged = true;
                                if(localValue[prop] != object[prop]) {
                                    localValue[prop] = object[prop];
                                    addAuthError('St.Peter : Unauthorized write blocked, ' +
                                        newPath, user);
                                }
                            } else {
                                modifyDocument.markModified(newPath);
                            }
                        }
                    } else {
                        localValue[prop] = object[prop];
                    }
                });
                return object;
            } else {
                addAuthError('St.Peter : Unauthorized write blocked, ' +
                    path, user);
            }
        }

        // convert document to javascript object for scanning children
        // (removes additional properties inherited from Document)
        var searchable = modifyDocument.get(originPath);

        if(typeof searchable == 'object') {
            if(value instanceof Array) {
                if(!(schema.userPermits.add &&
                    schema.userPermits.remove)) {
                        flagged = true;
                        addAuthError('St.Peter : Unauthorized array replacement, ' +
                            originPath, user);
                } else {
                    modifyDocument.set(originPath, passValue(value));
                }
            } else {
                if(searchable) {
                    if(typeof searchable.toObject == 'function') {
                        searchable = searchable.toObject({virtuals: true});
                    }
                    scanObject(searchable, schema, originPath, value);
                }
                if(!flagged || (flagged && user.ignore)) {
                    modifyDocument.set(originPath, passValue(value));
                }
            }
        } else {
            if(schema.userPermits[opPerm]) {
                modifyDocument.set(originPath, passValue(value));
            } else {
                addAuthError('St.Peter : Unauthorized write blocked, ' +
                    originPath, user);
            }
        }
    } else {

        /**
         * check origin permissions and execute non modify operations (add, remove
         * move)
         */

        if(schema.userPermits[opPerm]) {
            switch(op) {
                case 1:
                    modifyDocument.get(originPath).push(passValue(value));
                    break;
                case 2:
                    modifyDocument.get(originPath).splice(value, 1);
                    break;
                case 3:
                    modifyDocument.get(originPath).forEach((element, index, array) => {
                        if(element.toString() == passValue(value).toString()) {
                            array.splice(index,1);
                        }
                    });
                    break;
                case 4:
                    let array = modifyDocument.get(originPath);
                    let temp = array[value[0]];
                    let insertIndex = value[0]+value[1];

                    if(insertIndex >= array.length) {
                        insertIndex = insertIndex - array.length;
                    }
                    if(insertIndex < 0) {
                        insertIndex = array.length + insertIndex;
                    }

                    array.splice(value[0], 1);
                    array.splice(insertIndex, 0, temp);
                    break;
                case 5:
                    modifyDocument.get(originPath).forEach((element, index, array) => {
                        if(element._id.toString() == value.toString()) {
                            array.splice(index,1);
                        }
                    });
                    break;
            }
        } else {
            addAuthError('St.Peter : Unauthorized write blocked, ' +
                originPath, user);
        }
    }
}

module.exports.populateReplace = function(responseObject, req, user) {
    return new Promise((resolve, reject) => {
        var replaceActions = [];
        var promises = [];
        const scanKeys = function(object, path) {
            if(object) {
                Object.getOwnPropertyNames(object).forEach((oKey) => {
                    let newPath = ((path != '') ? (path + '.') : ('')) + oKey;
                    for(var rKey in req.body.replace) {
                        let found = false;
                        let searchKey = rKey;
                        let regex1 = new RegExp('(.*?)\#(.*)').exec(rKey);
                        if(regex1) {
                            if(regex1[2].length) {
                                searchKey = '^' + processPath(regex1[1]) + '.*?' + regex1[2];
                            } else {
                                searchKey = '^' + processPath(regex1[1]);
                            }
                            found = true;
                        }
                        if(searchKey.charAt(searchKey.length - 1) != '$') {
                            searchKey = searchKey + '\.?[0-9]?$';
                        }
                        if(newPath.search(searchKey)>=0) {
                            if(req.body.replace[rKey] instanceof Array) {
                                req.body.replace[rKey].forEach((field) => {
                                    replaceActions.push({
                                        key: rKey,
                                        field: field,
                                        refObject: object,
                                        refKey: oKey,
                                        path: newPath
                                    });
                                })
                            } else {
                                replaceActions.push({
                                    key: rKey,
                                    field: req.body.replace[rKey],
                                    refObject: object,
                                    refKey: oKey,
                                    path: newPath
                                });
                            }
                        }
                    }
                    if((typeof object[oKey] == 'object')) {
                        scanKeys(object[oKey], newPath);
                    }
                });
            }
        }

        if(responseObject) {
            var searchable = JSON.parse(JSON.stringify(responseObject));
            scanKeys(searchable, '');
        } else {
            resolve(null)
        }

        replaceActions.forEach((action) => {
            let object = action.refObject;
            let key = action.refKey;
            if(typeof action.field == 'string' && action.field.charAt(0) == '@') {
                let model = action.field.replace('@','').toLowerCase();
                if(object[key] instanceof Array) {
                    object[key].forEach((objectId, index) => {
                        promises.push(new Promise((resolveP, rejectP) => {
                            mongoose.model(model).findById(objectId, (err, doc) => {
                                object[key][index] =
                                    module.exports.checkAndRead('all', doc, user);
                                resolveP();
                            });
                        }).then(() => {
                            scanKeys(object[key][index], action.path + '.' + index);
                        }));
                    })
                } else {
                    promises.push(new Promise((resolveP, rejectP) => {
                        mongoose.model(model).findById(object[key], (err, doc) => {
                            object[key] =
                                module.exports.checkAndRead('all', doc, user);
                            resolveP();
                        });
                    }).then(() => {
                        scanKeys(object[key], action.path);
                    }));
                }
            }
        });

        Promise.all(promises).then(() => {
            replaceActions.forEach((action) => {

                let object = action.refObject;
                let key = action.refKey;
                if(action.field == '|$user') {
                    if(object[key]) {
                        object[key] =
                            (object[key]
                                .toString()
                                .indexOf(req.user._id) >= 0)
                    }
                }

                if(typeof action.field == 'object') {

                    let timeTest = action.field.valueType == 'Time';

                    let gt = action.field.comp == '>';
                    let lt = action.field.comp == '<';
                    let eq = action.field.comp == '=';

                    const timeTester = function(element) {

                        if(element == null) {
                            return false;
                        }

                        let op1 = null;
                        let op2 = null;

                        if(element[action.field.op1]) {
                            op1 = moment(element[action.field.op1]);
                        } else {
                            op1 = moment(action.field.op1);
                        }
                        if(element[action.field.op2]) {
                            op2 = moment(element[action.field.op2]);
                        } else {
                            op2 = moment(action.field.op2);
                        }

                        if(gt) {
                            return op1.isAfter(op2);
                        }
                        if(lt) {
                            return op1.isBefore(op2);
                        }
                        if(eq) {
                            return op1.isSame(op2);
                        }

                        return false;

                    };

                    const numberTester = function(element) {

                        let op1 = null;
                        let op2 = null;

                        if(element[action.field.op1]) {
                            op1 = parseInt(element[action.field.op1]);
                        } else {
                            op1 = parseInt(action.field.op1);
                        }
                        if(element[action.field.op2]) {
                            op2 = parseInt(element[action.field.op2]);
                        } else {
                            op2 = parseInt(action.field.op2);
                        }

                        if(gt) {
                            return op1 > op2;
                        }
                        if(lt) {
                            return op1 < op2;
                        }
                        if(eq) {
                            return op1 = op2;
                        }

                        return false;
                    }

                    object[key] = object[key].map((element) => {
                        if(timeTest) {
                            if(timeTester(element)) {
                                return element;
                            } else {
                                return null;
                            }
                        } else {
                            if(numberTester(element)) {
                                return element;
                            } else {
                                return null;
                            }
                        }
                    });
                }

                if(action.field == '-null') {
                    if(object[key]) {
                        object[key] =
                            (object[key].filter((val, index) => {
                                if(val != null) {
                                    val.itemIndex = index;
                                    return true;
                                }
                                return false;
                            }));
                    }
                }

            });
            resolve(searchable);
        });
    })
}

module.exports.checkModifyRespond = function(modifyDocument, req, res, rootPath) {

    if(modifyDocument == null) {
        console.log('No matching object found :', rootPath);
        return;
    }

    modifyDocument[symbols.original] = modifyDocument.toObject();

    if(rootPath == null) {
        rootPath = '';
    }

    var modified = false;
    var savePromise = Promise.resolve();
    var unauthorizedPaths = [];

    user = req.user.toObject();

    user.authErr = [];
    user.ignore = req.body.ignore;

    modifyDocument[symbols.errors] = [];

    if(req.body.modify && user) {
        for(var key in req.body.modify) {

            let fullKey = rootPath + key;
            let value = req.body.modify[key];

            module.exports.checkAndWrite(fullKey, modifyDocument, user, value);

        }

        savePromise = modifyDocument.save((err) => {
            if(err) {
                console.log(err);
            }
        });
    }

    savePromise.then(async (value) => {

        let response = [];
        let listenerEvents = [];

        if (req.body.get && user) {

            let listenersAdded = false;

            for (var key in req.body.get) {
                let fullKey = rootPath + key;
                var responseData = {};

                responseData[key] =
                    module.exports.checkAndRead(fullKey,
                        modifyDocument, user);

                if (req.body.replace) {
                    responseData =
                        await module.exports.populateReplace(responseData,
                            req, user);
                }

                if (req.body.get[key] != null) {
                    let listeners = modifyDocument.get('creeps');
                    let eventName = modifyDocument.get('_id') + '.' +
                                        processPath(fullKey, modifyDocument)
                                            .replace('.all','')
                                            .replace('all','')
                                            .replace('.unwind','');
                    let listener = user._id + ':' + eventName;

                    // add creep to the document if it's not there
                    if(!listeners.includes(listener)) {
                        listeners.push(listener);
                        listenersAdded = true;
                    }
                    // send the room name to the frontend
                    listenerEvents.push(eventName + ':' + req.body.get[key]);
                }
                response.push(responseData[key]);
            }
            // save added listeners
            if(listenersAdded) {
                modifyDocument.save();
            }
        }

        if(user.authErr.length) {
            res.status(401).json(user.authErr);
        } else {
            res.status(200).json({
                response: response,
                errors: modifyDocument[symbols.errors],
                listeners: listenerEvents
            });
        }
    });
}

module.exports.genericEndpoint = function(req, res, next) {

    if (req.body.findOne == null) {
        res.status(200).json({errors: ['Couldn\'t find findOne Object']});
        return;
    }

    /**
     * TODO: below section needs to be converted into a singular regex
     */
    var urlDecoded = req.url.match(/\/generic\/(.*)\/(.*)/);
    if(!urlDecoded) {
        urlDecoded = req.url.match(/\/generic\/(.*)/);
    }
    var collection = urlDecoded[1].toLowerCase();
    var subPath = null;
    if(urlDecoded[2]) {
        subPath = urlDecoded[2].replace('/','.');
    }

    if (!mongoose.modelNames().includes(collection)) {
        res.status(200).json({errors: ['Invalid collection name']});
        return;
    }

    var model = mongoose.model(collection);

    let getSubPath = Promise.resolve();
    let findOne = {};
    let subDocId = null;

    const passValue = function(value) {
        if (typeof value == 'object') {
            Object.getOwnPropertyNames(value).forEach((prop) => {
                if (value[prop]) {
                    value[prop] = passValue(value[prop]);
                }
            });
            return value;
        } else {
            if (typeof value == 'string') {
                if (value == '$user') {
                    value = new mongoose.Types.ObjectId(req.user._id);
                }
            }
            return value;
        }
    }

    req.body.findOne = passValue(req.body.findOne);

    if (subPath) {
        Object.getOwnPropertyNames(req.body.findOne).forEach((prop) => {
            findOne[subPath + '.' + prop] = req.body.findOne[prop];
        });
        getSubPath = new Promise((resolve, reject) => {
            model.findOne(findOne, {[subPath + '.$']: 1}, function(err, doc) {
                if (doc != null) {
                    subDocId = doc.get(subPath)[0]._id;
                }
                resolve();
            });
        });
    } else {
        findOne = req.body.findOne;
    }

    getSubPath.then(() => {
        model.findOne(findOne, function(err, doc) {
            if (doc == null) {
                res.status(200).json({errors: ['Document not found:' + findOne.toString()]})
                return;
            }
            if(subDocId != null) {
                let arrayIndex = null;
                doc.get(subPath).forEach((element, index) => {
                    if(element._id.toString() == subDocId.toString()) {
                        arrayIndex = index;
                    }
                });
                let fullSubPath = subPath + '.' + arrayIndex + '.';
                module.exports.checkModifyRespond(doc, req, res, fullSubPath);
            } else {
                if(subPath) {
                    res.status(200).json({errors: ['Sub document not found']});
                    return;
                }
                module.exports.checkModifyRespond(doc, req, res, subPath);
            }
        })
    })
};
