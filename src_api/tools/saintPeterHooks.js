/**
 * Global symbols
 */
const symbols = require('./globalSymbols');

/**
 * External controllers
 */
const socket = require('../controllers/socket')


const getArrayDiff = function(that, path) {

    let diffArray = {
        added: [],
        removed: []
    };

    let current = JSON.parse(JSON.stringify(that.get(path)));

	if (current instanceof Array && that[symbols.original]) {

        const arrayDiff = function(c, o, cIndex, oIndex) {
			let added = [];
			let removed = [];
			if(c[cIndex] && o[oIndex]) {
				let compare = idSort(c[cIndex], o[oIndex]);
				if(compare > 0) {
					removed.push(o[oIndex]);
					oIndex++;
				} else if(compare < 0) {
					added.push(c[cIndex].$index);
					cIndex++;
				} else {
					oIndex++;
					cIndex++;
				}
			} else {
				if(!o[oIndex]) {
					added.push(c[cIndex].$index);
					cIndex++;
				} else if(!c[cIndex]) {
					removed.push(o[oIndex]);
					oIndex++;
				}
			}
			if((oIndex<o.length) || cIndex<c.length) {
				let diff = arrayDiff(c, o, cIndex, oIndex);
				return {
					added: diff.added.concat(added),
					removed: diff.removed.concat(removed)
				}
			} else {
				return {
					added: added,
					removed: removed
				}
			}
		};

        const idSort = function(a, b) {
            if(a.$elem) a = a.$elem;
            if(b.$elem) b = b.$elem;
            let c,d;
            if(a._id && b._id) {
                c = a._id.toString();
                d = b._id.toString();
            } else {
                c = JSON.stringify(a);
                d = JSON.stringify(b);
            }
            return c.localeCompare(d);
        }

        /**
         * Map index to the array so that original position information is not
         * lost after sort
         */
        current = current.map((elem, index) => {
            return {
                $elem: elem,
                $index: index
            };
        });

		/**
		 * Cloning the original to ensure that hooks will
         * receive an unsorted original
		 */
		let original = JSON.parse(JSON.stringify(
                        path.split('.').reduce((object, prop) => {
							return object[prop];
						}, that[symbols.original])));

		current.sort(idSort);
		original.sort(idSort);

		diffArray = arrayDiff(current, original, 0, 0);

	} else {
        console.log('StPeter: Original document not found to calculate arrayDiff');
    }

    return diffArray;
}

module.exports = exports = function saintPeterHooks(schema, options) {

    var hooks = [];

    schema.pre('save', function(next) {

        // avoid hook algorithm execution for pre-save hooks called by sub-document
        // schemas
        if(!this.modifiedPaths().length) {
            next();
            return;
        }

    	var that = this;
    	var processedPaths = [];

    	const recursiveExecute = function(paths) {
    		return new Promise(function(resolve, reject) {
    			var modified = [];
    			var promises = [];
    			paths.forEach(function(element) {

    				processedPaths.push(element);
    				/**
    				 * Push the path as is returned by mongoose
    				 */
    				modified.push({path: element, indices: undefined, actual: element});

    				/**
    				 * Find occurences of array indices in the modified paths
    				 * eg: packages.2.options.0
    				 * would populate stars with [8,18] the location of the array indices
    				 * and populate indices with [2,0] the actual array indices
    				 */
    				var stars = [];
    				var indices = [];
    			    var starIndex = element.search('[.][0-9]');
    				while(starIndex >= 0) {
    					stars.push(starIndex + 1);
    					indices.push(parseInt(element.charAt(starIndex  + 1)));
    					var oldStarIndex = starIndex;
    					starIndex = element.slice(starIndex + 2).search('[.][0-9]');
    					starIndex = (starIndex >= 0) ? (starIndex + oldStarIndex + 2) : starIndex;
    				}

    				/**
    				 * Add new modified paths by replacing all combinations of array
    				 * indices with stars (*)
    				 * eg: packages.2.options.0 - will add
    				 * packages.*.options.0
    				 * packages.2.options.*
    				 * packages.*.options.*
    				 * to the modified array
    				 */
    				var iter = Math.pow(2, stars.length) - 1;
    				for(var i=1;i<=iter;i++) {
    					var newElement = element;
    					var newIndices = []
    					for(var j=0;j<stars.length;j++) {
    						if(Math.pow(2, j) & i) {
    							newElement = newElement.slice(0, stars[j])
    											+ '*'
    											+ newElement.slice(stars[j]+1, newElement.length);
    							newIndices.push(indices[j])
    						}
    					}
    					modified.push({path: newElement, indices: newIndices, actual: element});
    				}
    			});

    			/**
    		     * Call hooks that match any paths, including the additional
    			 * paths added by the earlier step which include stars (*)
    		     */
    			modified.forEach(function(element) {
    				promises.push(new Promise(function(resolveEx, rejectEx) {
    					let matched = false;
    				    for(var i=0;i<hooks.length;i++) {
    						for(var j=0;j<hooks[i].key.length;j++) {
    							if(hooks[i].key[j] == element.path) {
    								matched = true;
    								hooks[i].callback(that, j, element.indices, getArrayDiff(that, element.actual)).then(() => {
    									let newPaths = [];
    									that.modifiedPaths().forEach((element2) => {
    										if(!processedPaths.includes(element2)) {
    											newPaths.push(element2);
    										}
    									})
    									if(newPaths.length) {
    										recursiveExecute(newPaths).then(() => {
    											resolveEx();
    										});
    									} else {
    										resolveEx();
    									}
    								});
    								break;
    					        }
    						}
    				    }
    					if(!matched) {
    						resolveEx();
    					}
    				}));
    			});

    			Promise.all(promises).then(function() {
    				resolve();
    			});
    		});
    	};

    	recursiveExecute(this.modifiedPaths()).then(() => {
    		next();
            reactiveSync();
    	});

        const reactiveSync = function() {
            that.modifiedPaths().forEach((path) => {
                let creeps = that.get('creeps');
                creeps.forEach((listener) => {
                    let regex1 = new RegExp('(.*?)\:(.*)').exec(listener);
                    let regex2 = new RegExp('^([^.]+)\.(.*)').exec(regex1[2]);
                    if (regex2[2] == path) {
                        that.modifiedPaths().forEach((mpath) => {
                            let regex3 = new RegExp('^'+ path + '(.*)').exec(mpath);
                            if(regex3 && that.isDirectModified(mpath)) {
                                socket.socket.to(regex1[1]).clients((err, clients) => {
                                    if(clients.length) {
                                        socket.socket.to(regex1[1]).emit(regex1[2], {
                                            extension: regex3[1],
                                            own: (regex1[1] == user._id),
                                            diff: getArrayDiff(that, mpath)
                                        });
                                    } else {
                                        that.set('creeps', creeps.filter((l) => {
                                            return !(new RegExp('^' + regex1[1]).exec(l));
                                        }));
                                    }
                                });
                            }
                        })
                    }
                });
            });
        }
    });

    schema.statics.createHook  = function(key, callback) {
        hooks.push({key, callback});
    }

    schema.add({
        creeps: [String]
    });
}
