
/**
 * NOT IMPLEMENTED this file is an artifact from vebing-old and has not been
 * reimplemented
 * it is left in the repo as a reference for reimplementation
 * proper comments should be added after the reimplementation
 */
 
module.exports.globalEvents = [{
    name: 'Church',
    index: 0, // index denotes the array index which will hold budget/guests and other data for this particular event, Must be Unique
    ratio: 15,
    value: 0,
    breakdown: []
}, {
    name: 'Wedding',
    index: 1,
    ratio: 50,
    value: 0,
    breakdown: []
}, {
    name: 'Homecoming',
    index: 2,
    ratio: 35,
    value: 0,
    breakdown: []
}];

module.exports.globalFormats = [
    [1],
    [1 , 2],
    [0, 1],
    [0, 1, 2]
];
