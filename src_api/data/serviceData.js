/**
 * This file contains the default package options that will be used to initialize
 * the default package list upon vendor type selection
 */

module.exports.photographyOptions = [
    {
        optionKey: 0,
        optionType: 4, // predefined option format {{text1} {options}}
        defaultOption: true, // option added by default cannot be removed
        maxAllowed: 1, // 0 implies 1 per event, otherwise the number
        vendorSelected: true, // initially selected by default
        addOptions: true, // vendor can add text options
        numberProvidedVariable: false, // user can modify the
        selectedEvent: -1, // event selected for this option
        text1: 'Style :', // first text segment
        text2: '', // second text segment
        text3: '', // third text segment
        text4: '',
        timeConstrain: -1, // not time constrained
        selectedTCUnit: 0, // unit of the time constrain
        options: ['Candid', 'Artsy', 'Themed'],  // no predefined options
        selectedOption: 0,
        addonCost: 0, // no cost for addon
        numberProvided: -1, // no number
        numberProvidedUnit: '',
        rightOption: true,
        addons: []
    },
    {
        optionKey: 1,
        optionType: 4, // predefined option format {{text1} {options}}
        defaultOption: true, // option added by default cannot be removed
        maxAllowed: 1, // 0 implies 1 per event, otherwise the number
        vendorSelected: true, // initially selected by default
        addOptions: true, // vendor can add text options
        numberProvidedVariable: false, // user can modify the
        selectedEvent: -1, // event selected for this option
        text1: 'Shot in :', // first text segment
        text2: '', // second text segment
        text3: '', // third text segment
        text4: '',
        timeConstrain: -1, // not time constrained
        selectedTCUnit: 0, // unit of the time constrain
        options: ['Normal', 'Ultra-Wide-Angle', 'Typical-Wide-Angle', 'Normal'],  // no predefined options
        selectedOption: 0,
        addonCost: 0, // no cost for addon
        numberProvided: -1, // no number
        numberProvidedUnit: '',
        rightOption: true,
        addons: []
    },
    {
        optionKey: 2,
        optionType: 0, // predefined option format {{text1} {options}}
        defaultOption: true, // option added by default cannot be removed
        maxAllowed: 1, // 0 implies 1 per event, otherwise the number
        vendorSelected: true, // initially selected by default
        addOptions: true, // vendor can add text options
        numberProvidedVariable: false, // user can modify the
        selectedEvent: -1, // event selected for this option
        text1: 'Key Photographers :', // first text segment
        text2: '', // second text segment
        text3: '', // third text segment
        text4: '',
        timeConstrain: -1, // not time constrained
        selectedTCUnit: 0, // unit of the time constrain
        options: ['Kasun Hunt', 'Gobbi Samarathunga'],  // no predefined options
        selectedOption: -1,
        addonCost: 0, // no cost for addon
        numberProvided: -1, // no number
        numberProvidedUnit: '',
        rightOption: false,
        addons: []
    },
    {
        optionKey: 3,
        optionType: 1, // predefined option format {{timeConstrain} {timeConstrainUnit} {text1} {events} {text2} {numberProvided} {numberProvidedUnit}}
        defaultOption: true, // option added by default cannot be removed
        maxAllowed: 0, // 0 implies 1 per event, otherwise the number
        vendorSelected: false, // initially selected by default
        addOptions: true, // vendor can add text options
        numberProvidedVariable: false, // user can modify the
        selectedEvent: -1, // event selected for this option
        text1: 'of coverage of the', // first text segment
        text2: 'by', // second text segment
        text3: 'hour(s) of additional coverage of the', // third text segment
        text4: 'additional photographer(s) for the',
        timeConstrain: 4, // not time constrained
        selectedTCUnit: 0, // unit of the time constrain
        options: [],  // no predefined options
        selectedOption: -1,
        addonCost: 0, // no cost for addon
        numberProvided: 2, // number
        numberProvidedUnit: 'Photographer(s)',
        rightOption: false,
        addons: [{
            optionType: 10,
            modifies: 'timeConstrain'
        },
        {
            optionType: 11,
            modifies: 'numberProvided'
        }]
    },
    {
        optionKey: 4,
        optionType: 2, // predefined option format {{numberProvided} {numberProvidedUnit} {text1}}
        defaultOption: true, // option added by default cannot be removed
        maxAllowed: 1, // 0 implies 1 per event, otherwise the number
        vendorSelected: false, // initially selected by default
        addOptions: true, // vendor can add text options
        numberProvidedVariable: false, // user can modify the
        selectedEvent: -1, // event selected for this option
        text1: 'edited and retouched provided digitally', // first text segment
        text2: '', // second text segment
        text3: '', // third text segment
        text4: '',
        timeConstrain: -1, // not time constrained
        selectedTCUnit: 0, // unit of the time constrain
        options: [],  // no predefined options
        selectedOption: -1,
        addonCost: 0, // no cost for addon
        numberProvided: 300, // number
        numberProvidedUnit: 'pictures',
        rightOption: false,
        addons: [{
            optionType: -1,
            modifies: 'numberProvided'
        }]
    },
    {
        optionKey: 5,
        optionType: 3, // predefined option format {{numberProvided} {numberProvidedUnit} {text1} {options}}
        defaultOption: false, // option added by default cannot be removed
        maxAllowed: -1, // 0 implies 1 per event, otherwise the number
        vendorSelected: false, // initially selected by default
        addOptions: true, // vendor can add text options
        numberProvidedVariable: false, // user can modify the
        selectedEvent: -1, // event selected for this option
        text1: 'of size', // first text segment
        text2: '', // second text segment
        text3: '', // third text segment
        text4: '',
        timeConstrain: -1, // not time constrained
        selectedTCUnit: 0, // unit of the time constrain
        options: ['12x12','10x10','15x10'],  // no predefined options
        selectedOption: -1,
        addonCost: 0, // no cost for addon
        numberProvided: 2, // number
        numberProvidedUnit: 'Album(s)',
        rightOption: false,
        addons: [{
            optionType: -1,
            modifies: 'numberProvided'
        }]
    },
    {
        optionKey: 6,
        optionType: 3, // predefined option format {{numberProvided} {numberProvidedUnit} {text1} {options}}
        defaultOption: false, // option added by default cannot be removed
        maxAllowed: -1, // 0 implies 1 per event, otherwise the number
        vendorSelected: false, // initially selected by default
        addOptions: true, // vendor can add text options
        numberProvidedVariable: false, // user can modify the
        selectedEvent: -1, // event selected for this option
        text1: 'of size', // first text segment
        text2: '', // second text segment
        text3: '', // third text segment
        text4: '',
        timeConstrain: -1, // not time constrained
        selectedTCUnit: 0, // unit of the time constrain
        options: ['12x12','10x10','15x10'],  // no predefined options
        selectedOption: 0,
        addonCost: 0, // no cost for addon
        numberProvided: 2, // number
        numberProvidedUnit: 'Framed Portrait(s)',
        rightOption: false,
        addons: [{
            optionType: -1,
            modifies: 'numberProvided'
        }]
    },
    {
        optionKey: 7,
        optionType: 3, // predefined option format {{numberProvided} {numberProvidedUnit} {text1} {options}}
        defaultOption: false, // option added by default cannot be removed
        maxAllowed: -1, // 0 implies 1 per event, -1 implies infinite
        vendorSelected: false, // initially selected by default
        addOptions: true, // vendor can add text options
        numberProvidedVariable: false, // user can modify the
        selectedEvent: -1, // event selected for this option
        text1: 'of size', // first text segment
        text2: '', // second text segment
        text3: '', // third text segment
        text4: '',
        timeConstrain: -1, // not time constrained
        selectedTCUnit: 0, // unit of the time constrain
        options: ['5\'x3\'','4\'x2\'','3\'x3\''],  // no predefined options
        selectedOption: 0,
        addonCost: 0, // no cost for addon
        numberProvided: 1, // number
        numberProvidedUnit: 'Canvas Print(s)',
        rightOption: false,
        addons: [{
            optionType: -1,
            modifies: 'numberProvided'
        }]
    },
    {
        optionKey: 20,
        optionType: 5, // predefined option format {{numberProvided} {numberProvidedUnit} {text1} {options}}
        defaultOption: false, // option added by default cannot be removed
        maxAllowed: -1, // 0 implies 1 per event, -1 implies infinite
        vendorSelected: false, // initially selected by default
        addOptions: false, // vendor can add text options
        numberProvidedVariable: false, // user can modify the
        selectedEvent: -1, // event selected for this option
        text1: 'Custom option..', // first text segment
        text2: '', // second text segment
        text3: '', // third text segment
        text4: '',
        timeConstrain: -1, // not time constrained
        selectedTCUnit: 0, // unit of the time constrain
        options: [],  // no predefined options
        selectedOption: 0,
        addonCost: 0, // no cost for addon
        numberProvided: 1, // number
        numberProvidedUnit: '',
        rightOption: false,
        addons: [{
            optionType: -1,
            modifies: 'numberProvided'
        }]
    }
]


module.exports.photographyPackages = [
    {
        name: 'Sample Package 1',
        description: 'This is a sample package that you should modify to create your own',
        price: {val:500, currency: '$'},
        rating: 5,
        format: 0,
        options: module.exports.photographyOptions.filter(
            option => [0,1,2,3,4,5].includes(option.optionKey))
    },
    {
        name: 'Sample Package 2',
        description: 'This is a sample package that you should modify to create your own',
        price: {val:1000, currency: '$'},
        rating: 5,
        format: 1,
        options: module.exports.photographyOptions.filter(
            option => [0,1,2,3,4,5,6,7].includes(option.optionKey))
    },
    {
        name: 'Sample Package 3',
        description: 'This is a sample package that you should modify to create your own',
        price: {val:500, currency: '$'},
        rating: 5,
        format: 2,
        options: module.exports.photographyOptions.filter(
            option => [0,1,2,3,4].includes(option.optionKey))
    }
];

module.exports.photographyNewPackage = {
    name: 'New Package',
    description: 'Write a small description for this package..',
    price: {val:100, currency: '$'},
    rating: 5,
    format: 0,
    options: module.exports.photographyOptions.filter(
        option => option.defaultOption)
};
