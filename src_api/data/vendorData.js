module.exports.defaults = {
    /**
     * Initial navigator elements to be added to a user associated with a vendor
     */
    elements: [
        {
            name: 'Profile',
            url: '/vendor/board/profile'
        },
        {
            name: 'Dashboard',
            url: '/vendor/board/dashboard'
        },
        {
            name: 'Packages',
            url: '/vendor/board/packages'
        },
        {
            name: 'Gallery',
            url: '/vendor/board/gallery'
        },
        {
            name: 'Calendar',
            url: '/vendor/board/calendar'
        }
    ],
    /**
     * Set of initial cards in the vendor profile
     */
    profile: [
        {
            name: 'cover-card',
            data: null
        },
        {
            name: 'seperator-card',
            data: {
                rgb: [0,0,0,]
            }
        }
    ]
}

module.exports.vendorTypes = [
    'Photographer',
    'Venue',
    'Catering',
    'Video',
    'Suits',
    'Dresses',
    'DJ',
    'Band',
    'Jewellery',
    'Deco',
    'Dressing',
    'Wedding Cars',
    'Cakes'
]

module.exports.formatOptions = [
    'Ceremony',
    'Ceremony + Reception',
    'Reception'
]
