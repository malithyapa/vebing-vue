module.exports.defaults = {
    /**
     * Initial navigator elements to be added to a user associated with a vendor
     */
    elements: [
        {
            name: 'Dashboard',
            url: '/planner/board/ni'
        },
        {
            name: 'Setup',
            url: '/planner/board/ni'
        },
        {
            name: 'Budget',
            url: '/planner/board/ni'
        },
        {
            name: 'Guests',
            url: '/planner/board/ni',
        },
        {
            name: 'Collaborators',
            url: '/planner/board/ni'
        },
        {
            name: 'Shortlisted',
            url: '/planner/board/shortlisted'
        },
        {
            name: 'Browse',
            url: '/planner/board/packages',
            searchable: true
        }
    ]
}
