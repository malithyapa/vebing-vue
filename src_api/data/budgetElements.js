
/**
 * NOT IMPLEMENTED this file is an artifact from vebing-old and has not been
 * reimplemented
 * it is left in the repo as a reference for reimplementation
 * proper comments should be added after the reimplementation
 */

module.exports.global = [
    {
        name: 'Photography',
        ratio: 20
    }, {
        name: 'Venue & Catering',
        ratio: 35
    }, {
        name: 'Flowers',
        ratio: 10
    }, {
        name: 'Dresses',
        ratio: 10
    }, {
        name: 'Suits',
        ratio: 7
    }, {
        name: 'Makeup',
        ratio: 7
    }, {
        name: 'Cake',
        ratio: 5
    }, {
        name: 'Cards',
        ratio: 3
    }, {
        name: 'Drinks',
        ratio: 6
    }
];
