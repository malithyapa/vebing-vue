/**
 * Module dependencies.
 */
var express = require('express');
var passport = require('passport');
var router = express.Router();


/**
 * Internal tool libraries
 */
var stPeter = require('../tools/saintPeter');

//var jwt = require('express-jwt');
var formidable = require('express-formidable');

/**
 * Initialize formidable middleware for handling multipart form data
 */
var fileUpload = formidable({
    uploadDir: './uploads/',
    multiples: true
})

/**
 * auth - passport middleware for handling jwt authentication
 */
var auth = passport.authenticate('jwt', {session: false});

/**
 * Other referenced controllers
 */
var vendorBoards = require('../controllers/vendor/vendor_board_apis');
var plannerBoards = require('../controllers/planner/planner_board_apis');

var ctrlVendor = require('../controllers/vendor/vendor_generic_apis');
var vendorCoverCard = require('../controllers/vendor/covercard_apis');
var vendorContactCard = require('../controllers/vendor/contactcard_apis');
var vendorPackageCard = require('../controllers/vendor/packagecard_apis');
var vendorAlbumCard = require('../controllers/vendor/albumcard_apis');

var ctrlAuth = require('../controllers/user/authentication_apis');
var ctrlUser = require('../controllers/user/user_generic_apis');
var ctrlCalendar = require('../controllers/calendar/calendar_generic_apis');
var ctrlImages = require('../controllers/images/image_generic_apis');

/**
 * allowCrossDomain - expressjs middleware for allowing cross domain requests
 * this is required for the development setup because the front-end is served
 * through a different server
 */
allowCrossDomain = function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Authorization, Content-Length, X-Requested-With');
	if ('OPTIONS' === req.method) {
		res.send(200);
	} else {
		next();
	}
};
router.use(allowCrossDomain);

/**
 * vebing API definition
 */

// START - COMMON APIs

/**
 * Generic API endpoint for stpeter
 */
router.post('/generic/*', auth, stPeter.genericEndpoint);

/**
 * Common APIs - POST /register/
 * registers a new user with the local strategy
 * @param {Request Body} {JSON(
 *      name: name,
 *      email: email,
 *      password: password,
 *      gender: gender, (?could be removed)
 * )}
 * @returns {Response Status} HTTP status code
 */
router.post('/register', ctrlAuth.register);

/**
 * Common APIs - POST /activate?id=uuid
 * validates the email of a registered user
 * @param {Request URL} id uuid for activation
 * @returns {Response Status} HTTP status code
 */
router.get('/activate', ctrlAuth.activate);

/**
 * Common APIs - POST /login/
 * registers a new user with the local strategy
 * @param {Request Body} {JSON(
 *      email: email,
 *      password: password
 * )}
 * @returns {Response Status} HTTP status code
 * @returns {Object} JWT token
 */
router.post('/login', ctrlAuth.login);

/**
 * Common APIs - POST /facebook/
 * registers a new user with the facebook api
 * @param {Request Body} {JSON(
 *      name: name,
 *      email: email,
 *      gender: gender, (?could be removed)
 * )}
 * @returns {Response Status} HTTP status code
 * @returns {Object} JWT token
 */
router.post('/facebook', ctrlAuth.facebook);

/**
 * Common APIs - POST /validate
 * checks if the jwt token is valid
 * @param {Request Header} {Authorization: bearer _jwttoken_}
 * @returns {Response Status} HTTP status code
 */
router.post('/validate', auth, (req, res) => {
    res.status(200).json({});
});

/**
 * Common APIs - GET /elements
 * returns the navigation elements for the current user
 * @param {Request Header} {Authorization: bearer _jwttoken_}
 * @returns {Object} navigation elements and board api urls for the current user
 * @returns {Response Status} HTTP status code
 */
router.get('/elements', auth, ctrlUser.getElements);

/**
 * Common APIs - GET /user
 * generic api for getting and modifying self information
 * @param {Request Header} {Authorization: bearer _jwttoken_}
 * @param {Request Body} {JSON(
 *     userId: user ID
 * )}
 * @returns {Object} user data requested by the user
 * @returns {Response Status} HTTP status code
 */
router.post('/user/generic', auth, ctrlUser.userGeneric);

/**
 * Common APIs - POST /images/generic
 * updates properties of existing images
 * @param {Request Header} {Authorization: bearer _jwttoken_}
 * @param {Request Body} {JSON(
 *     imageId: image ID
 * )}
 * @returns {Object} image data requested by the user
 * @returns {Response Status} HTTP status code
 */
router.post('/images/generic', auth, ctrlImages.imageGeneric);

/**
 * Common APIs - POST /images/generic
 * updates properties of existing images
 * @param {Request Header} {Authorization: bearer _jwttoken_}
 * @param {Request Body} {JSON(
 *     imageId: image ID
 * )}
 * @returns {Object} image data requested by the user
 * @returns {Response Status} HTTP status code
 */
router.post('/package/generic', auth, ctrlVendor.packageGeneric);

// END - COMMON API
//
// START - PLANNER API

/**
 * Planner Board APIs - GET /planner/board/_boardname_
 * @param {Request Header} {Authorization: bearer _jwttoken_}
 * @returns {Object} card names and basic card data for _boardname_
 * @returns {Response Status} HTTP status code
 */
router.get('/planner/board/ni', auth, plannerBoards.getNi);
router.get('/planner/board/shortlisted', auth, plannerBoards.getShortlisted);
router.get('/planner/board/packages', auth, plannerBoards.getPackages);
router.post('/planner/board/findpackages', auth, plannerBoards.findPackages);

/**
 * /user/*
 * NOT IMPLEMENTED
 * below are artifacts from vebing-old and has to be reimplemented
 */
// router.get('/setup', auth, ctrlProject.getSetup);
// router.post('/setup/format', auth, ctrlProject.setSetupFormat);
// router.post('/setup/dates', auth, ctrlProject.setSetupDates);
// router.post('/setup/budget', auth, ctrlProject.setSetupBudget);
// router.post('/setup/guests', auth, ctrlProject.setSetupGuests);
// router.get('/budget', auth, ctrlProject.getBudget);
// router.post('/budget', auth, ctrlProject.saveBudget);
// router.get('/budget/elements', auth, ctrlProject.getBudgetElements);
// router.post('/partner', auth, ctrlPartner.setPartner);
// router.get('/partner', auth, ctrlPartner.getPartner);
// router.get('/guest', auth, ctrlGuest.getGuests);
// router.post('/guest', auth, ctrlGuest.addGuest);

// END - PLANNER API
//
// START - VENDOR API

 /**
  * Vendor Board APIs - GET /vendor/board/_boardname_
  * @param {Request Header} {Authorization: bearer _jwttoken_}
  * @returns {Object} card names and basic card data for _boardname_
  * @returns {Response Status} HTTP status code
  */
router.get('/vendor/board/profile', auth, vendorBoards.getProfile);
router.post('/vendor/board/profile', auth, vendorBoards.setProfile);
router.get('/vendor/board/dashboard', auth, vendorBoards.getDashboard);
router.get('/vendor/board/packages', auth, vendorBoards.getPackages);
router.get('/vendor/board/gallery', auth, vendorBoards.getGallery);
router.get('/vendor/board/calendar', auth, vendorBoards.getCalendar);

/**
 * Vendor generic API - GET /vendor/generic
 * This endpoint can be used to get and modify any data in the
 * vendor collection as per the St.Peter spec
 * @param {Request Header} {Authorization: bearer _jwttoken_}
 * @returns {Object} data requested by the frontend
 * @returns {Response Status} HTTP status code
 */
router.post('/vendor/generic', auth, ctrlVendor.vendorGeneric);

/**
 * Vendor Card APIs - GET /vendor/card/_cardname_
 * only implemented for vendors accessing their own cards
 * NOT IMPLEMENTED for users accessing vendor cards
 * @param {Request Header} {Authorization: bearer _jwttoken_}
 * @returns {Object} card data for _cardname_
 * @returns {Response Status} HTTP status code
 */
router.post('/vendor/card/packagecard/image', auth, fileUpload,
    ctrlImages.saveAndResizeImage, vendorPackageCard.addImage);

/**
 * Vendor Card APIs - GET /vendor/card/_cardname_
 * only implemented for vendors accessing their own cards
 * NOT IMPLEMENTED for users accessing vendor cards
 * @param {Request Header} {Authorization: bearer _jwttoken_}
 * @returns {Object} card data for _cardname_
 * @returns {Response Status} HTTP status code
 */
router.post('/vendor/card/albumcard/image', auth, fileUpload,
    ctrlImages.saveAndResizeImage, vendorAlbumCard.addImage);

/**
 * Vendor Card APIs - POST /vendor/card/_cardname_/_imagename_
 * uploads a new picture to be assigned to _imagename_
 * resizes the image and returns the saved path
 * @param {Request Header} {Authorization: bearer _jwttoken_}
 * @param {Request Body} {multipart/form-data}
 * @returns {String} saved image path
 * @returns {Response Status} HTTP status code
 */
router.post('/vendor/card/covercard/dp', auth, fileUpload,
    ctrlImages.saveAndResizeImage, vendorCoverCard.saveDp);
router.post('/vendor/card/covercard/cover', auth, fileUpload,
    ctrlImages.saveAndResizeImage, vendorCoverCard.saveCover);
router.post('/image', auth, fileUpload, ctrlImages.saveAndResizeImage);

/**
 * Calendar generic API - GET /calendar/generic
 * This endpoint can be used to get and modify any data in the
 * calendar collection as per the St.Peter spec
 * @param {Request Header} {Authorization: bearer _jwttoken_}
 * @returns {Object} data requested by the frontend
 * @returns {Response Status} HTTP status code
 */
router.post('/calendar/generic', auth, ctrlCalendar.calendarGeneric);

module.exports = router;
