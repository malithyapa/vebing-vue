import textedit_sl from '@/src_client/components/utility/textedit_sl.vue'
import { shallowMount } from '@vue/test-utils'

describe('textedit_sl;', () => {
    it('should render the text that is passed to it as a prop', () => {
        const wrapper = shallowMount(textedit_sl, {
            propsData: {
                text: 'This is some text'
            }
        });
        expect(wrapper.find('.text-display').text())
        .toEqual('This is some text')
    });

    it('should hide the text field and show the text editor when clicked', () => {
        const wrapper = shallowMount(textedit_sl, {
            propsData: {
                text: 'More text'
            }
        });
        wrapper.find('.text-display').trigger('click');
        expect(wrapper.find('.text-display').isVisible()).toBe(false);
        expect(wrapper.find('.text-edit').isVisible()).toBe(true);
    })

    it('should indicate the original text in the text editor', () => {
        const wrapper = shallowMount(textedit_sl, {
            propsData: {
                text: 'More text'
            }
        });
        wrapper.find('.text-display').trigger('click');
        expect(wrapper.find('.text-edit').element.value).toContain('More text');
    })

    it('should show the blinking bar while the network request is pending', () => {
        const wrapper = shallowMount(textedit_sl, {
            propsData: {
                text: 'More text'
            }
        });
        wrapper.setData({ updating: true });
        expect(wrapper.find('.updating-line').exists()).toBe(true);
    })

})
